import gulp from "gulp";
import cp from "child_process";
import gutil from "gulp-util";
import BrowserSync from "browser-sync";
import webpack from "webpack";
import webpackConfig from "./webpack.conf";
import svgstore from "gulp-svgstore";
import svgmin from "gulp-svgmin";
import inject from "gulp-inject";
import cleanCSS from "gulp-clean-css";
import concat from "gulp-concat";
import uglify from "gulp-uglify";
import terser from "gulp-terser";
import gulpLoadPlugins from 'gulp-load-plugins';

const $ = gulpLoadPlugins()
const browserSync = BrowserSync.create();
const hugoBin = `./bin/hugo.${process.platform === "win32" ? "exe" : process.platform}`;
const defaultArgs = ["-d", "../dist", "-s", "site"];

const onError = (err) => {
    console.log(err)
}

if (process.env.DEBUG) {
  defaultArgs.unshift("--debug")
}

gulp.task("hugo", (cb) => buildSite(cb));
gulp.task("hugo-preview", (cb) => buildSite(cb, ["--buildDrafts", "--buildFuture"]));
gulp.task("build", ["sass", "js", "hugo"]);
gulp.task("build-preview", ["sass", "js", "hugo-preview"]);

gulp.task('sass', () => {
    return gulp.src([
        'src/sass/**/*.scss'
    ])
    .pipe($.sass({ precision: 5 }))
    .pipe($.autoprefixer(['ie >= 10', 'last 2 versions']))
    .pipe($.size({ gzip: true, showFiles: true }))
    .pipe(cleanCSS({debug: true}, (details) => {
      console.log(`${details.name}: ${details.stats.originalSize}`);
      console.log(`${details.name}: ${details.stats.minifiedSize}`);
    }))
    .pipe(gulp.dest('site/static/css'))
    .pipe(browserSync.stream())
})


// gulp.task("js", (cb) => {
//   const myConfig = Object.assign({}, webpackConfig);
//
//   webpack(myConfig, (err, stats) => {
//     if (err) throw new gutil.PluginError("webpack", err);
//     gutil.log("[webpack]", stats.toString({
//       colors: true,
//       progress: true
//     }));
//     browserSync.reload();
//     cb();
//   });
// });

//'src/js/**/*.js'

gulp.task("js", () => {
    return gulp.src([
        'node_modules/jquery/dist/jquery.slim.min.js',
        'node_modules/bootstrap/dist/js/bootstrap.bundle.min.js'
    ])
    //.pipe($.plumber({ errorHandler: onError }))
    // .pipe($.print())
    //.pipe($.babel())
    .pipe($.concat('scripts.js'))
    .pipe($.uglify())
    .pipe($.size({ gzip: true, showFiles: true }))
    .pipe(gulp.dest('site/static/js'))
})

gulp.task("svg", () => {
  const svgs = gulp
    .src("site/static/img/icons-*.svg")
    .pipe(svgmin())
    .pipe(svgstore({inlineSvg: true}));

  function fileContents(filePath, file) {
    return file.contents.toString();
  }

  return gulp
    .src("site/layouts/partials/svg.html")
    .pipe(inject(svgs, {transform: fileContents}))
    .pipe(gulp.dest("site/layouts/partials/"));
});

gulp.task("server", ["hugo", "sass", "js", "svg"], () => {
  browserSync.init({
    server: {
      baseDir: "./dist"
    }
  });
  gulp.watch("./src/js/**/*.js", ["js"]);
  gulp.watch("./src/sass/**/*.scss", ["sass"]);
  gulp.watch("./site/static/img/icons-*.svg", ["svg"]);
  gulp.watch("./site/**/*", ["hugo"]);
});

function buildSite(cb, options) {
  const args = options ? defaultArgs.concat(options) : defaultArgs;

  return cp.spawn(hugoBin, args, {stdio: "inherit"}).on("close", (code) => {
    if (code === 0) {
      browserSync.reload("notify:false");
      cb();
    } else {
      browserSync.notify("Hugo build failed :(");
      cb("Hugo build failed");
    }
  });
}

// ---------- OLD GULPFILE ---------

// gulp.task('server', ['build'], () => {
//     gulp.start('init-watch')
//     $.watch(['archetypes/**/*', 'data/**/*', 'content/**/*', 'layouts/**/*', 'static/**/*', 'config.toml'], () => gulp.start('hugo'))
// });
//
// gulp.task('server:with-drafts', ['build-preview'], () => {
//     gulp.start('init-watch')
//     $.watch(['archetypes/**/*', 'data/**/*', 'content/**/*', 'layouts/**/*', 'static/**/*', 'config.toml'], () => gulp.start('hugo-preview'))
// });
//
// gulp.task('init-watch', () => {
//     suppressHugoErrors = true;
//     browserSync.init({
//         server: {
//             baseDir: 'public'
//         },
//         open: false,
//         notify: false
//     })
//     $.watch('src/sass/**/*.scss', () => gulp.start('sass'))
//     $.watch('src/js/**/*.js', () => gulp.start('js-watch'))
//     $.watch('src/images/**/*', () => gulp.start('images'))
// })
//
// gulp.task('build', () => {
//     runSequence(['sass', 'js', 'fonts', 'images', 'pub-delete'], 'hugo')
// })
//
// gulp.task('build-preview', () => {
//     runSequence(['sass', 'js', 'fonts', 'images', 'pub-delete'], 'hugo-preview')
// })
//
//
//
// gulp.task('hugo', (cb) => {
//     let baseUrl = process.env.NODE_ENV === 'production' ? process.env.URL : process.env.DEPLOY_PRIME_URL;
//     let args = baseUrl ? ['-b', baseUrl] : [];
//
//     return spawn('hugo', args, { stdio: 'inherit' }).on('close', (code) => {
//         if (suppressHugoErrors || code === 0) {
//             browserSync.reload()
//             cb()
//         } else {
//             console.log('hugo command failed.');
//             cb('hugo command failed.');
//         }
//     })
// })
//
//
//
// gulp.task('hugo-preview', (cb) => {
//     let args = ['--buildDrafts', '--buildFuture'];
//     if (process.env.DEPLOY_PRIME_URL) {
//         args.push('-b')
//         args.push(process.env.DEPLOY_PRIME_URL)
//     }
//     return spawn('hugo', args, { stdio: 'inherit' }).on('close', (code) => {
//         if (suppressHugoErrors || code === 0) {
//             browserSync.reload()
//             cb()
//         } else {
//             console.log('hugo command failed.');
//             cb('hugo command failed.');
//         }
//     })
// })
//
// // --
//
//
//
// gulp.task('js-watch', ['js'], (cb) => {
//     browserSync.reload();
//     cb();
// });
//
// gulp.task('js', () => {
//     return gulp.src([
//         'node_modules/jquery/dist/jquery.slim.min.js',
//         'node_modules/bootstrap/dist/js/bootstrap.min.js',
//         'src/js/**/*.js'
//     ])
//     .pipe($.plumber({ errorHandler: onError }))
//     .pipe($.print())
//     //.pipe($.babel())
//     .pipe($.concat('scripts.js'))
//     .pipe($.uglify())
//     .pipe($.size({ gzip: true, showFiles: true }))
//     .pipe(gulp.dest('static/js'))
// })
//
// gulp.task('fonts', () => {
//     return gulp.src('src/fonts/**/*.{woff,woff2}')
//         .pipe(gulp.dest('static/fonts'));
// });
//
// gulp.task('images', () => {
//     return gulp.src('src/images/**/*.{png,jpg,jpeg,gif,svg,webp,ico}')
//         .pipe($.newer('static/images'))
//         .pipe($.print())
//         .pipe($.imagemin())
//         .pipe(gulp.dest('static/images'));
// });
//
// gulp.task('cms-delete', () => {
//     return del(['static/admin'], { dot: true })
// })
//
// gulp.task('pub-delete', () => {
//     return del(['public/**', '!public'], {
//       // dryRun: true,
//       dot: true
//     }).then(paths => {
//       console.log('Files and folders deleted:\n', paths.join('\n'), '\nTotal Files Deleted: ' + paths.length + '\n');
//     })
// })
