---
title: Rent Stabilized Rider
excerpt: >-
  The rent stabilized rider should include: The name of the tenant as well as the address of the unit, The signature of the tenant and owner
slug: rent-stabilized-rider
order: 1
---

**Rent-Stabilized Rider**
The rent stabilized rider should include:
- The name of the tenant as well as the address of the unit
- The signature of the tenant and owner
- Information on the prior rent and a summary of how the rent was calculated, including individual apartment improvements (IAIs)
- A list of the tenant’s rights under the Rent Stabilization law including their right to request supporting documentation for IAIs.

For more information on the Rent-Stabilized Rider, see [Fact Sheet #2](http://www.nyshcr.org/Rent/FactSheets/orafac2.pdf).

**Initial Lease.** For all rent-stabilized units when tenants receive either an ***initial lease*** or any ***lease renewals***, the landlord must include a ["New York City Lease Rider for Rent Stabilized Tenants"](http://www.nyshcr.org/forms/rent/ralr1.pdf). This rider (which simply means attachment) specifies the prior rent, the reason for a rent increase, and outlines the rights of the tenant. Although landlords can receive fines or sanctions if they don’t include the rider, it is very common. Tenants should ask if the unit is rent-stabilized before they sign the initial lease, as well as ask for a rent history.

**Lease Renewals.** Those who live in a rent-stabilized apartment are guaranteed to receive [a lease renewal](http://www.nyshcr.org/Forms/Rent/rtp8.pdf) option of either 1 or 2 years. The tenant should receive the renewal lease 150 to 90 days before your current lease expires, and then has 60 days to return the signed lease renewal.


It is up to the tenant to decide whether to sign a 1-or 2-year lease or to tell their landlord that they will not be renewing the lease.

**How to Access a Unit’s Rent History.** The New York State Division of Housing and Community Renewal (DHCR) has information on [obtaining the rent history for your unit](http://www.nyshcr.org/rent/tenantresources.htm). The easiest way is to fill out [this online form](https://portal.hcr.ny.gov/app/ask), and DHCR will send a copy of the unit’s rent history directly to the unit.

Tenants can also call the `New York State Division of Housing and Community Renewal (DHCR)` at `718-739-6400,` or [visit them online here](http://www.nyshcr.org/AboutUs/ContactUs.htm). The DHCR is the organization charged with overseeing rent regulation in the city.
