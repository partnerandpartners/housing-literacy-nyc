---
title: Rider
description: Lease Rider
date: 2015-01-01
publishdate: 2015-01-01
slug: rider
menu: main
name: “Lease Rider”
weight: 3
---
<div class="page">
  <div style= "color: #9B9B9B" class="text-center">
    Page 1 of 10
    </div>
  <div class="mt-3 text-center">
State of New York <br />
Division of Housing and Community Renewal Office of Rent Administration<br />
Gertz Plaza<br />
92-31 Union Hall Street<br />
Jamaica, New York 11433<br />
Web Site: www.nyshcr.org<br />
Email address: rentinfo@nyshcr.org<br />
<strong>Revision Date: March 2016</strong>
  </div>

  <div class="mt-3 text-center">
    <a class="annotation-link" data-toggle="modal" data-target="#rent-stabilized-rider">New York City LEASE Rider For Rent Stabilized Tenants</a><br />
  <strong>FAILURE BY AN OWNER TO ATTACH A COPY OF THIS RIDER TO THE TENANT’S
  LEASE WITHOUT CAUSE MAY RESULT IN A FINE OR OTHER SANCTIONS</strong>

  </div>

  <div class="mt-3">
    <strong>NOTICE</strong>
 &emsp; This Rider, with this Notice, must be attached to all vacancy and renewal leases for rent stabilized apartments. This Rider was prepared pursuant to Section 26-511(d) of the New York City Rent Stabilization Law.

 &emsp; This Rider must be in a print size larger than the print size of the lease to which the Rider is attached. The following language must appear in bold print upon the face of each lease: <strong>“ATTACHED RIDER SETS FORTH RIGHTS AND OBLIGATIONS OF TENANTS AND LANDLORDS UNDER THE RENT STABILIZATION LAW.”</strong>
  </div>

  <div class="border p-3 p-sm-4 p-xl-5 mt-3">
  <a class="annotation-link" data-toggle="modal" data-target="#sections-to-complete">Section 1</a> (If this is a renewal lease, do not complete section 1, go to section 2)
  </div>

  <div class="mt-3">
 &emsp; If Box A is checked, the owner <strong> MUST</strong> show how the rental amount provided for in such vacancy lease has been computed above the previous legal regulated rent by completing the following chart. The owner is not entitled to a rent which is more than the legal regulated rent. For additional information see DHCR Fact Sheet #5.
 &emsp; In addition, the owner <strong> MUST</strong> complete the Notice To Tenant Disclosure of Bedbug Infestation History, as required by the NYC Housing Maintenance Code Section 27-2018.1, which is required to be served on the tenant with this Lease Rider.

ANY INCREASE ABOVE THE PREVIOUS LEGAL REGULATED RENT MUST BE IN ACCORDANCE WITH ADJUSTMENTS PERMITTED BY THE RENT GUIDELINES BOARD AND THE RENT STABILIZATION CODE.
<strong>VACANCY LEASE RENT CALCULATION</strong>
 &emsp; <a class="annotation-link" data-toggle="modal" data-target="#prior-unit-status"> Status of Apartment and Last Tenant</a> (Owner to Check Appropriate Box - (A), (B), (C), or (D).)

&emsp; &#9744; (A) This apartment was rent stabilized when the last tenant moved out. If the last stabilized tenancy was more than 4 years prior to the signing of this lease see RSC 2526.1(a)(3)(iii) or DHCR Fact Sheet # 5 which may entitle the Owner to additional rent guideline increases over the last stabilized tenancy.

<a class="annotation-link" data-toggle="modal" data-target="#address-of-rent-stabilized-unit">Address: </a><span class="fake-underline">________________________________________</span> Apt #: <span class="fake-underline">___________________</span>
</div>

<div class="row">
  <div class="col-8">
  1.  Previous Legal Regulated Rent <br/>
<a class="annotation-link" data-toggle="modal" data-target="#additional-rent-guidelines-increases">(i) Additional Rent Guideline increases, applicable only, if the last stabilized
tenancy was more than 4 years prior to the signing of this lease.</a>
</div>
  <div class="col-4">$___________</span></div>
</div>

<div class="row mt-3">
  <div class="col-8">
  2.  <a class="annotation-link" data-toggle="modal" data-target="#statutory-vacancy-increases">Statutory Vacancy Increase</a> <br />
  (i)  Increase based on (1 year) or (2 year) lease or
     <a class="annotation-link" data-toggle="modal" data-target="#i-limits-to-vacancy-increase-due-to-preferential-rent">(Preferential Rent Vacancy Limitation) </a>. Circle one.	  (________%)
</div>
  <div class="col-4">$___________</div>
</div>

<div class="row mt-3">
  <div class="col-8">
  (ii) <a class="annotation-link" data-toggle="modal" data-target="#ii-limits-to-vacancy-increase-due-to-preferential-rent">Increase based on length of time </a> (8 years or more)
      since last vacancy allowance or if no vacancy allowance
      has been taken, the number of years that the apartment
      has been subject to stabilization. 		(0.6% x number of years)
</div>
  <div class="col-4">$___________</div>
</div>

<div class="row mt-3">
  <div class="col-8">
  (iii) <a class="annotation-link" data-toggle="modal" data-target="#iii-additional-increase-due-to-low-rent">Increase based on low rental amount. </a> If applicable
       complete (a) or (b), but not both.

      (a) Previous legal regulated rent was <a class="annotation-link" data-toggle="modal" data-target="#iiia-additional-increase-due-to-low-rent">less than $300 - </a>
            additional $100 increase, enter 100

      (b) If the previous legal regulated rent was <a class="annotation-link" data-toggle="modal" data-target="#iiib-additional-increase-due-to-low-rent">$300 or more
            but less than $500	</a>					(1)_________
            the sum of (i) and (ii)					(2)_________
           (1) minus (2). If less than zero, enter zero <br />
           Amount from line (3)			$______________
</div>
  <div class="col-4">$___________</div>
</div>

<div class="row mt-3">
  <div class="col-8">
   3.  <a class="annotation-link" data-toggle="modal" data-target="#vacancy-allowance"> Vacancy Allowance,</a> if permitted by NYC Rent Guidelines Board (_____%)
 </div>
  <div class="col-4">$___________</div>
</div>

<div class="row mt-3">
  <div class="col-8">
  4.   Guidelines <a class="annotation-link" data-toggle="modal" data-target="#supplementary-adjustments">Supplementary Adjustment</a>, if permitted by NYC Rent
       Guidelines Board
 </div>
  <div class="col-4">$___________
  </div>
</div>

</div>

<div class="page">
  <div style= "color: #9B9B9B" class="text-center">
    Page 2 of 10
    </div>
<div class="row mt-3">
  <div class="col-8">
5.   Individual Apartment Improvements (IAI) <br>
  <a class="annotation-link" data-toggle="modal" data-target="#individual-apartment-improvement-requests-for-verification">Tenant Request for Documentation</a>
 <br /> &emsp; &#9744; Check the box if you want to request at this time, from the owner, copies of documentation (e.g.,bills, invoices, cancelled checks, etc.) that clarify and support the individual apartment improvement(s) cost detailed in this rider.<strong> If you do not request it now, you have the lawful right to request it within 60 days of the execution of the lease, by certified mail and the owner must then provide the documentation within 30 days either by certified mail or by personal delivery with a signed acknowledgement receipt by tenant. (Refer to Rider Section 3, Provision 4 - Other Rent Increases, Individual Apartment Improvements.)</strong>

 </div>
  <div class="col-4">$___________
  </div>
</div>

<div class="row mt-3">
  <div class="col-8">
  <strong><a class="annotation-link" data-toggle="modal" data-target="#iai-items-to-be-verified">Items</a> <br />
  A.	Bathroom Renovation (check all applicable items)</strong><br />
   &emsp; &#9744; Complete Renovation (if this box is checked you are not required to check Individual Items) <br />
   &emsp; &emsp; 	OR <br />
   &emsp; &#9744; Individual Items <br />
   &emsp; (Check all applicable items)<br />
   &emsp; &#9744; Sink <br />
   &emsp; &#9744; Shower Body<br />
   &emsp; &#9744; Toilet<br />
   &emsp; &#9744; Tub<br />
   &emsp; &#9744; Plumbing<br />
   &emsp; &#9744; Cabinets<br />
   &emsp; &#9744; Vanity<br />
   &emsp; &#9744; Floors and/or Wall Tiles<br />
   &emsp; &#9744; Other (describe) ______________	 <br /> <a class="annotation-link" data-toggle="modal" data-target="#total-costs-and-total-rent-increases"> Total Costs for Parts and Labor		____________________ <br /> <br />

  * Total Rent Increase (1/40th or 1/60th)	____________________</a> <br />
 &emsp; &emsp; &emsp; &emsp; (A)
 </div>
  <div class="col-4">$______________
  </div>
</div>

<div class="row mt-3">
  <div class="col-8">
<strong>  B.	Kitchen Renovation (check all applicable items)</strong>
<br />

 &emsp; &#9744; Complete Renovation (if this box is checked you are not required to check Individual Items)<br />
 &emsp; &emsp; <strong>OR</strong><br />
 &emsp; &#9744; Individual Items<br />
(Check all applicable items)<br />
 &emsp; &#9744; Sink<br />
 &emsp; &#9744; Stove<br />
 &emsp; &#9744; Refrigerator<br />
 &emsp; &#9744; Dishwasher<br />
 &emsp; &#9744; Cabinets<br />
 &emsp; &#9744; Plumbing<br />
 &emsp; &#9744; Floor and/or Wall Tiles Countertops<br />
 &emsp; &#9744; Other (describe) _____________	<br />
Total Costs for Parts and Labor		____________________ <br /> <br />

* Total Rent Increase (1/40th or 1/60th)	____________________ <br />
 &emsp; &emsp; &emsp; &emsp; (B)


 </div>
  <div class="col-4">$______________
  </div>
</div>

<div class="row mt-3">
  <div class="col-8">
<strong> C.	Other (check all applicable items)</strong> <br />

  &emsp; &#9744; Doors <br />
  &emsp; &#9744; Windows <br />
  &emsp; &#9744; Radiators <br />
  &emsp; &#9744; Light Fixtures <br />
  &emsp; &#9744; Electrical Work <br />
  &emsp; &#9744; Sheetrock <br />
  &emsp; &#9744; Other (describe) _____________  <br />
Total Costs for Parts and Labor		____________________   <br /> <br />

* Total Rent Increase (1/40th or 1/60th)	____________________   <br />
 &emsp; &emsp; &emsp; &emsp; (C)  <br /> <br />

<a class="annotation-link" data-toggle="modal" data-target="#total-individual-apartment-improvement-increase">
  $____________________ <br />
 <strong> Total IAI Rent Increase <br />
  Sum of (A)(B) and (C) </strong> <br /> <br />
  </a>

  *  1/40th if the building has 35 or fewer units. 1/60th if the building is over 35 units.


 </div>
  <div class="col-4">$______________
  </div>
</div>
</div>

<div class="page">
  <div style= "color: #9B9B9B" class="text-center">
    Page 3 of 10
    </div>
  <div class="row">
    <div class="col">
      6. New Legal Regulated Rent
    </div>
    <div class="col">
      <a class="annotation-link" data-toggle="modal" data-target="#new-legal-regulate-rent">$____________</a>
    </div>
    <div class="col">
    </div>
  </div>

  <div class="row">
    <div class="col">
      * 6A. Preferential Rent
    </div>
    <div class="col">
      $____________
    </div>
    <div class="col">
      $____________
      (if charged)
    </div>
  </div>



  <div class="row">
    <div class="col">
      7. Air Conditioner Surcharges:
    </div>
    <div class="col">
    </div>
    <div class="col">
      <a class="annotation-link" data-toggle="modal" data-target="#separate-charges">$____________</a>
    </div>
  </div>

  <div class="row">
    <div class="col">
      8. Appliance Surcharges (Tenant installed washer, dryer, dishwasher)
    </div>
    <div class="col">
    </div>
    <div class="col">
      <a class="annotation-link" data-toggle="modal" data-target="#separate-charges">$____________</a>
    </div>
  </div>

  <div class="row">
    <div class="col">
      9. Ancillary Services charged (e.g., garage)
    </div>
    <div class="col">
    </div>
    <div class="col">
      <a class="annotation-link" data-toggle="modal" data-target="#separate-charges">$____________</a>
    </div>
  </div>

  <div class="row">
    <div class="col">
      10. Other (specify) ____________________
    </div>
    <div class="col">
    </div>
    <div class="col">
      <a class="annotation-link" data-toggle="modal" data-target="#new-tenants-total-monthly-payment">$____________</a>
    </div>
  </div>

  <div class="row">
    <div class="col">
      11. New Tenant’s Total Payment
    </div>
    <div class="col">
    </div>
    <div class="col">
      <a class="annotation-link" data-toggle="modal" data-target="#new-tenants-total-monthly-payment">$____________</a>
    </div>
  </div>
<div class="mt-3">
  * If a “preferential rent” is being charged, please read Provision # 20 of this Rider. <br />

&#9744; (B) This apartment was   <a class="annotation-link" data-toggle="modal" data-target="#after-rent-control">Rent Controlled</a> at the time the last tenant moved out. This tenant is the  first rent stabilized tenant and the rent agreed to and stated in the lease to which this Rider is attached is $________. The owner is entitled to charge a market rent to the  rst rent stabilized tenant. The  rst rent charged to the  rst rent stabilized tenant becomes the initial legal regulated rent for the apartment under the rent stabilization system. However, if the tenant has reason to believe that this rent exceeds a “fair market rent”, the tenant may  le a “Fair Market Rent Appeal” with DHCR. The owner is required to give the tenant notice, on DHCR Form RR-1, of the right to  le such an appeal. The notice must be served by certified mail. A tenant only has 90 days, after such notice was mailed to the tenant by the owner by certified mail, to  le an appeal. Otherwise, the rent set forth on the registration form becomes the initial legal regulated rent.
</div>

<div class="row">
  <div class="col">
&#9744; (C) The rent for this apartment is an <a class="annotation-link" data-toggle="modal" data-target="#government-program-status">Initial or Restructured Rent pursuant to a Government Program</a>. (Specify
Program_________) <br/> <br/> -or-
  </div>
  <div class="col">
    $____________
  </div>
</div>

<div class="row mt-3">
  <div class="col">
&#9744; <a class="annotation-link" data-toggle="modal" data-target="#other-status">  (D) Other___________________________________</a>
  </div>
  <div class="col">$____________
  </div>
</div>

<div class="mt-3">
  (Specify - for example, a market or “ rst” rent after renovation to an individual apartment where the outer dimensions of the apartment have been substantially altered.)
  </div>

  <div class="border p-3 p-sm-4 p-xl-5 mt-3 text-center">
    <strong>
  <a class="annotation-link" data-toggle="modal" data-target="#section-2-vacancy-and-renewal-leases">Section 2 - This section needs to be completed for vacancy and renewal leases</a>
    </strong>
  </div>
<div class="mt-3">
  <a class="annotation-link" data-toggle="modal" data-target="#address-for-the-rent-stabilized-unit">Lease Rider for the housing accommodation: </a> <br />
  (Print Housing Accommodation Address and Apartment Number) <br />
  <a class="annotation-link" data-toggle="modal" data-target="#dates">Lease Start Date: </a> _____________________________ 	<br />
  Lease End Date: _____________________________ <br />
  Lease Dated: ________________________________

</div>

<div class="mt-3">
  The tenant named in the lease hereby acknowledges the contemporaneous receipt of the above lease rider for the housing accommodation stated above.<br />
  __________________________________________<br />
  <a class="annotation-link" data-toggle="modal" data-target="#tenants-acknowledgement-of-receipt-of-lease-rider">Print Name of Tenant(s) </a> <br />
  __________________________________________<br />
  <a class="annotation-link" data-toggle="modal" data-target="#landlords-acknowledgement-of-receipt-of-lease-rider">Signature(s) and Date</a>
</div>

<div class="mt-3">
Subject to penalties provided by law, the owner of the housing accommodation hereby certi es that the above rider is hereby contemporaneously provided to the tenant with the signing of the lease and the information provided by the owner herein is true and accurate based on its records.<br />
__________________________________________<br />
Print Name of Owner or Owner’s Agent<br />
__________________________________________<br />
Signature(s) and Date
</div>

</div>

<div class="page">
  <div style= "color: #9B9B9B" class="text-center">
    Page 4 of 10
    </div>
<div class="border p-3 p-sm-4 p-xl-5 mt-3 text-center">
  <a class="annotation-link" data-toggle="modal" data-target="#provisions">Section 3</a> - PROVISIONS
</div>

<div class="mt-3">
  <strong>INTRODUCTION:</strong> <br />
This Rider is issued by the New York State Division of Housing and Community Renewal (“DHCR”), pursuant to the Rent Stabilization Law (“RSL”), and Rent Stabilization Code (“RSC”). It generally informs tenants and owners about their basic rights and responsibilities under the RSL.<br />  <br />

This Rider does not contain every rule applicable to rent stabilized apartments. It is only informational and its provisions are not part of and do not modify the lease. However, it must be attached as an addendum to the lease.<br /> <br />

It does not otherwise replace or modify more exact or complete sections of the RSL, the RSC, any order of DHCR, or any order of the New York City Rent Guidelines Board that govern this tenancy.<br />  <br />

The Appendix lists organizations which can provide assistance to tenants and owners who have inquiries, complaints or requests relating to subjects covered in this Rider.<br /> <br />

Tenants should keep a copy of this Rider and of any lease they sign.
</div>

<div class="mt-3">
<strong>1. GUIDELINES INCREASES FOR RENEWAL LEASES</strong> <br />

The owner is entitled to increase the rent when a tenant renews a lease (“renewal lease”). Each year, effective October 1, the New York City Rent Guidelines Board sets the percentage of maximum permissible increase over the immediately preceding September 30th rent for leases which will begin during the year for which the guidelines order is in effect. The date a lease starts determines which guidelines order applies.  <br /> <br />

Guidelines orders provide increases for Renewal Leases. The renewing tenant has the choice of the
length of the lease. Different percentages are set for rent increases for leases of 1 or 2 years. The guidelines order may incorporate additional provisions, such as a supplementary low-rent adjustment. For additional information see DHCR Fact Sheet #26.
</div>

<div class="mt-3">
<strong>2. 	VACANCY INCREASES FOR VACANCY LEASES</strong> <br />

The owner is entitled to increase the previous legal regulated rent when a new tenant enters into a lease (“vacancy lease”). The legal regulated rent immediately preceding the vacancy may be increased by statutory vacancy increases as follows: <br /> <br />

If the vacancy lease is for a term of 2 years, 20% of the previous legal regulated rent; or if the vacancy lease is for a term of 1 year, the increase shall be 20% of the previous legal regulated rent less an amount equal to the difference between: <br />

&emsp; a) The 2 year renewal lease guideline promulgated by the New York City Rent Guidelines Board (“RGB”) applied to the prior legal regulated rent and <br />

&emsp; b) The 1 year renewal lease guideline promulgated by the RGB applied to the prior legal regulated rent.<br /> <br />

The Rent Act of 2015 modi ed the vacancy allowance that an owner can add to the legal regulated rent when the vacating tenant was paying a preferential rent. If a vacating tenant was paying a preferential rent, the vacancy lease rent increase that can be applied to the vacating tenant’s legal rent will be limited to 5% if the last vacancy lease commenced less than two years ago, 10% if less than three years ago, 15% if less than four years ago and 20% if four or more years ago.<br /> <br />

Additional increases are available to owners where the legal regulated rent was last increased by a vacancy allowance eight or more years prior to the entering into the subject vacancy lease or if no vacancy allowance has been taken, the number of years that the apartment has been subject to stabilization. Generally, this increase equals 0.6%, multiplied by the prior legal regulated rent, multiplied by the number of years since the last vacancy increase.<br /> <br />

If the prior legal regulated rent was less than $300, the total vacancy increase shall be as calculated above, plus an additional $100. If the prior legal regulated rent was at least $300, and no more than $500, in no event shall the total vacancy increase be less than $100.<br /> <br />

A RGB order may authorize an additional vacancy “allowance,” which is separate from the statutory vacancy increase which an owner may charge. The tenant has the choice of whether the vacancy lease will be for a term of 1 or 2 years. For additional information, see DHCR Fact Sheets #5 and 26. <br /> <br />

Pursuant to the Rent Act of 2011, effective June 24, 2011, owners can charge and collect no more than one (1) vacancy lease rent increase in a calendar year (January 1st through December 31st).
</div>

<div class="mt-3">
<strong>3.	 <a class="annotation-link" data-toggle="modal" data-target="#security-deposit">SECURITY DEPOSITS</a></strong><br />
An owner may collect a security deposit no greater than one month’s rent. However, if the present tenant moved into the apartment prior to the date the apartment  rst became rent stabilized, and the owner collected more than one month’s rent as security, the owner may continue to retain a security deposit of up to two months’ rent for that tenant only. When the rent is increased, the owner may charge an additional amount to bring the security deposit up to the full amount of the increased rent to which the owner is entitled.<br /> <br />

A security deposit must be deposited in an interest bearing trust account in a banking organization in New York State. The tenant has the option of applying the interest to the rent, leaving the interest in the bank or receiving the interest annually. For additional information see DHCR Fact Sheet #9.
</div>

<div class="mt-3">
<strong>4. 	OTHER RENT INCREASES</strong> <br />
In addition to guidelines and statutory vacancy increases, the rent may be permanently increased based upon the following:<br />  <br />

<strong>(A) Individual Apartment Improvements (“IAI”)</strong> - Where an owner installs a new appliance in, or makes an improvement to an apartment, the owner may be entitled to increase the rent of that apartment for the new appliance or improvement. If an apartment has a tenant in occupancy, the owner can only receive a rent increase for the individual apartment improvement if the tenant consents in writing to pay an increase for the improvement (s). However, if the apartment is vacant, tenant consent is not required.<br />  <br />

Pursuant to the Rent Act of 2011, effective September 24, 2011, in buildings that contain more than 35 apartments, the owner can collect a permanent rent increase equal to 1/60th of the cost of the Individual Apartment Improvement (IAI). In buildings that contain 35 apartments or less, the owner can collect a permanent rent increase equal to 1/40th of the cost of the IAI, as had previously been allowed.<br /> <br />

For example, if a new dishwasher is installed in a vacant apartment, in a 100-unit building, and the cost is $900, the rent can be increased by $15 (1/60th of $900). The same installation in a 20-unit building would result in a $22.50 rent increase (1/40th of $900). The increase, if taking place on a vacancy, is added to the legal rent after the application of the statutory vacancy increase, not before. (See Fact Sheet # 12 for additional information.)<br /> <br />

<strong>The Rent Code Amendments of 2014 require that the DHCR Lease Rider offered to vacancy lease tenants contain noti cation to the tenant of the right to request from the owner by certified mail Individual Apartment Improvements (IAI’s) supporting documentation at the time the lease is offered or within 60 days of the execution of the lease. The owner shall provide such documentation within 30 days of that request in person or by certified mail. A tenant who is not provided with that documentation upon demand may  le form RA-90 “Tenant’s Complaint of Owner’s Failure to Renew Lease and/or Failure to Furnish a copy of a Signed Lease” to receive a DHCR Order that directs the furnishing of the IAI supporting documentation. (Refer to Rider Section 1, Individual Apartment Improvements.)</strong> <br /> <br />

<strong>(B) <a class="annotation-link" data-toggle="modal" data-target="#mcis">Major Capital Improvements (“MCI”)</a></strong> - An owner is permitted a rental increase for building-wide major capital improvements, such as the replacement of a boiler, or new plumbing. The owner must receive approval from DHCR. The Rent Act of 2015 requires DHCR to compute the rent increase based upon an eight-year period of amortization for buildings with 35 or fewer apartments and a nine-year period for buildings with more than 35 apartments. The owner is not required to obtain tenant consent. Tenants are served with a notice of the owner’s application and have a right to challenge the MCI application on certain grounds. For additional information see DHCR Fact Sheet #24.<br /> <br />

<strong>(C) Hardship</strong> - An owner may apply to increase the rents of all rent stabilized apartments based on hardship when: <br /> <br />
&emsp; 1. the rents are not sufficient to enable the owner to maintain approximately the same average annual net income for a current three-year period as compared with the annual net income which prevailed on the average over the period 1968 through 1970, or for the first three years of operation if the building was completed since 1968, or for the first three years the owner owned the building if the owner cannot obtain records for the years 1968-1970; or <br>

&emsp; 2. where the annual gross rental income does not exceed the annual operating expenses by a sum equal to at least 5% of such gross income.<br /> <br />

If an application for a rent increase based on a major capital improvement or hardship is granted, the owner may charge the increase during the term of an existing lease only if the lease contains a clause speci cally authorizing the owner to do so.<br /> <br />

An increase based on a major capital improvement or hardship may not exceed 6% in any 12 month period. Any increase authorized by DHCR which exceeds these annual limitations may be collected in future years.
</div>

<div class="mt-3">
<strong>5. 	<a class="annotation-link" data-toggle="modal" data-target="#rent-registration">RENT REGISTRATION</a></strong> <br />
(A) <strong> Initial </strong> <br />
An owner must register an apartment’s rent and services with DHCR within 90 days from when the apartment  rst becomes subject to the RSL. To complete the rent registration process, the owner must serve the tenant’s copy of the registration statement upon the tenant. The tenant may challenge the correctness of the rental as stated in the registration statement within 90 days of the certi ed mailing to the tenant of the tenant’s copy of the registration statement.<br /> <br />

(B)<strong> Annual </strong> <br />
The annual update to the initial registration must be  led with DHCR by July 31st with information as of April 1st of each year. At the time of such  ling, the owner must provide each tenant with the tenant’s copy. The rental amounts registered annually are challengeable by the  ling with DHCR of a “Tenant’s Complaint of Rent Overcharge and/or Excess Security Deposit” (DHCR Form RA-89). In general, the rental history that precedes the 4 year period prior to the filling of the complaint will not be examined. The Rent Code Amendments of 2014 do however, provide for certain exceptions, including histories involving preferential rents.<br /> <br />

(C)<strong> Penalties</strong> <br />
Failure to register shall bar an owner from applying for or collecting any rent increases until such registration has occurred, except for those rent increases which were allowable before the failure to register. However, treble damages will not be imposed against an owner who collects a rent increase, but has not registered where the overcharge results solely because of such owner’s failure to  le a timely or proper initial or annual registration statement. Where the owner  les a late registration statement, any rent increase collected prior to the late registration that would have been lawful except for the failure to timely and properly register will not be found to be an overcharge.
</div>
</div>

<div class="page">
  <div style= "color: #9B9B9B" class="text-center">
    Page 5 of 10
    </div>
<div class="mt-3">
<strong>6.  RENEWAL LEASES</strong><br />
A tenant has a right to a renewal lease, with certain exceptions (see provision 10 of this Rider, “When An Owner May Refuse To Renew A Lease”).<br /> <br />

At least 90 days and not more than 150 days before the expiration of a lease, the owner is required to notify the tenant in writing that the lease will soon expire. That notice must also offer the tenant the choice of a 1 or 2 year lease at the permissible guidelines increase. After receiving the notice, the tenant always has 60 days to accept the owner’s offer, whether or not the offer is made within the above time period, or even beyond the expiration of the lease term.<br /> <br />

Any renewal lease, except for the amount of rent and duration of its term, is required to be on the same terms and conditions as the expired lease, and a fully executed copy of the same must be provided to the tenant within 30 days from the owner’s receipt of the renewal lease or renewal form signed by the tenant. If the owner does not return a copy of such fully executed Renewal Lease Form to the tenant within 30 days of receiving the signed renewal lease from the tenant, the tenant is responsible for payment of the new lease rent and may  le a “Tenant’s Complaint of Owner’s Failure to Renew Lease and/or Failure to Furnish a Copy of a Signed Lease” (DHCR Form RA-90). DHCR shall order the owner to furnish the copy of the renewal lease or form. If the owner does not comply within 20 days of such order, the owner shall not be entitled to collect a rent guidelines increase until the lease or form is provided.<br /> <br />

If a tenant wishes to remain in occupancy beyond the expiration of the lease, the tenant may not refuse to sign a proper renewal lease. If the tenant does refuse to sign a proper renewal lease, he or she may be subject to an eviction proceeding.<br /> <br />

<a class="annotation-link" data-toggle="modal" data-target="#clauses-that-can-be-added-to-tenants-prior-lease">An owner may add to a renewal lease the following clauses even if such clauses were not included in the tenant’s prior lease:</a><br /> <br />

(A)  the rent may be adjusted by the owner on the basis of Rent Guidelines Board or DHCR Orders;<br /> <br />

(B)  if the owner or the lease grants permission to sublet or assign, the owner may charge a sublet allowance for a sub-tenant or assignee, provided the prime lease is a renewal lease. However, this sublet allowance may be charged even if such clause is not added to the renewal lease. (Subletting is discussed in provision 9 of this Rider); <br /> <br />

(C)  (1) if the building in which the apartment is located is receiving tax bene ts pursuant to Section 421-a of the Real Property Tax Law, a clause may be added providing for an annual or other periodic rent increase over the initial rent at an average rate of not more than 2.2 % of the amount of such initial rent per annum not to exceed nine 2.2 percent increases. Such charge shall not become part of the legal regulated rent; however, the cumulative 2.2 percent increases charged prior to the termination of tax benefits may continue to be collected as a separate charge; <br /> <br />

(2)   provisions for rent increases if authorized under Section 423 of the Real Property Tax Law: a clause may be added to provide for an annual or other periodic rent increase over the legal regulated rent if authorized by Section 423 of the Real Property Tax Law;  <br /> <br />

(D)  if the Attorney General, pursuant to Section 352-eeee of the General Business Law, has accepted for  ling an Eviction Plan to convert the building to cooperative or condominium ownership, a clause may be added providing that the lease may be cancelled upon expiration of a 3-year period after the Plan is declared effective. (The owner must give the tenant at least 90 days notice that the 3-year period has expired or will be expiring.)<br /> <br />

(E)  if a proceeding based on an Owner’s Petition for Decontrol (“OPD”) is pending, a clause may be added providing that the lease will no longer be in effect as of 60 days from the issuance of a DHCR Decontrol Order, or if a Petition for Administrative Review (“PAR”) is  led against such order, 60 days from the issuance of a DHCR order dismissing or denying the PAR, (see provision 17 of this Rider, “Renewal Leases Offered During Pendency of High Income Deregulation Proceedings”).
</div>

<div class="mt-3">
<strong>7.  <a class="annotation-link" data-toggle="modal" data-target="#succession-rights">RENEWAL LEASE SUCCESSION RIGHTS</a></strong>  <br />
In the event that the tenant has permanently vacated the apartment at the time of the renewal lease offer, family members who have lived with the tenant in the apartment as a primary residence for at least two years immediately prior to such permanent vacating (one year for family members who are senior citizens and disabled persons), or from the inception of the tenancy or commencement of the relationship, if for less than such periods, are entitled to a renewal lease.<br /> <br />

“Family Member” includes the spouse, son, daughter, stepson, stepdaughter, father, mother, stepfather, step- mother, brother, sister, grandfather, grandmother, grandson, granddaughter, father-in-law, mother-in-law, son-in-law or daughter-in-law of the tenant.<br /> <br />

“Family member” may also include any other person living with the tenant in the apartment as a primary residence who can prove emotional and financial commitment and interdependence between such person and the tenant. Examples of evidence which is considered in determining whether such emotional and  financial commitment and interdependence existed are set forth in the Rent Stabilization Code. Renewal lease succession rights are also discussed in detail in DHCR Fact Sheet #30.
</div>
</div>

<div class="page">
  <div style= "color: #9B9B9B" class="text-center">
    Page 6 of 10
    </div>
<div class="mt-3">
<strong>8. 	<a class="annotation-link" data-toggle="modal" data-target="#decrease-in-services">SERVICES</a></strong> <br />
Written notification to the owner or managing agent should be given but is <strong>NOT</strong> required, before  ling a decrease in service complaint with DHCR. Owners who have not received prior written noti cation from the tenant will however, be given additional time to respond to a complaint  led with DHCR. Applications based on a lack of heat or hot water must be accompanied by a report from the appropriate city agency.<br /> <br />

All Emergency conditions, do not require prior written notification. These include but are not limited to: vacate order (5 day notification),  re (5 day notification), no water apartment wide, no operable toilet, collapsed or collapsing ceiling or walls, collapsing  door, no heat/hot water apartment wide (violation required), broken or inoperative apartment front door lock, all elevators inoperable, no electricity apartment wide, window to  re escape (does not open), water leak (cascading water, soaking electrical  xtures), window-glass broken (not cracked), broken/unusable  re escapes, air conditioner broken (summer season). Complaints to DHCR on the appropriate DHCR form that cite any of these emergency conditions will be treated as a  first priority and will be processed as quickly as possible. It is recommended that tenants use a separate DHCR form for any problematic conditions that are not on this emergency condition list.<br /> <br />

Certain conditions, examples of which are set forth in the Rent Stabilization Code, which have only a minimal impact on tenants, do not affect the use and enjoyment of the premises, and may exist despite regular maintenance of services. These conditions do not rise to the level of a failure to maintain required services. The passage of time during which a disputed service was not provided without complaint may be considered in determining whether a condition is de minimis. For this purpose, the passage of 4 years or more will be considered presumptive evidence that the condition is de minimis.<br /> <br />

The amount of any rent reduction ordered by DHCR shall be reduced by any credit, abatement or offset in rent which the tenant has received pursuant to Sec. 235-b of the Real Property Law (“Warranty of Habitability”) that relates to one or more conditions covered by the DHCR Order. For additional information see DHCR Fact Sheets #3, #14 and #37.
</div>

<div class="mt-3">
<strong>9. 	<a class="annotation-link" data-toggle="modal" data-target="#subletting-and-assigning">SUBLETTING AND ASSIGNMENT</a></strong> <br />
A tenant has the right to sublet his/her apartment, even if subletting is prohibited in the lease, provided that the tenant complies strictly with the provisions of Real Property Law Section 226-b. Tenants who do not comply with these requirements may be subject to eviction proceedings. Compliance with Section 226-b is not determined by DHCR, but by a court of competent jurisdiction. If a tenant in occupancy under a renewal lease sublets his/her apartment, the owner may charge the tenant, the sublet allowance provided by the NYC Rent Guidelines Board. This charge may be passed on to the sub-tenant. However, upon termination of the sublease, the Legal Regulated Rent shall revert to the Legal Regulated Rent without the sublet allowance. The rent increase is the allowance provided by the NYC Rent Guidelines Board available when the tenant’s renewal lease commenced, and it takes effect when the subletting takes place. If a tenant in occupancy under a vacancy lease sublets, the owner is not entitled to any rent increase during the subletting. <br /> <br />

A tenant who sublets his/her apartment is entitled to charge the sub-tenant the rent permitted under the Rent Stabilization Law, and may charge a 10% surcharge payable to the tenant only if the apartment sublet is fully furnished with the tenant’s furniture. Where the tenant charges the sub-tenant any additional rent above such surcharge and sublet allowance, if applicable, the tenant shall be required to pay to the sub-tenant a penalty of three times the rent overcharge, and may also be required to pay interest and attorney’s fees. The tenant may also be subject to an eviction proceeding. <br /> <br />

<strong>Assignment of Leases</strong> <br />

In an assignment, a tenant transfers the entire remainder of his or her lease to another person (the assignee), and gives up all of his/her rights to reoccupy the apartment.<br /> <br />

Pursuant to the provisions of Real Property Law Section 226-b, a tenant may not assign his/her lease without the written consent of the owner, unless the lease expressly provides otherwise. If the owner consents to the assignment of the lease, the owner may charge the assignee, a vacancy allowance, the rent the owner could have charged had the renewal lease been a vacancy lease. Such vacancy allowance shall remain part of the Legal Regulated Rent for any subsequent renewal lease. The rent increase is the vacancy allowance available when the tenant’s renewal lease commenced and it takes effect when the assignment takes place.<br /> <br />

An owner is not required to have reasonable grounds to refuse to consent to the assignment. However, if the owner unreasonably refuses consent, the owner must release the tenant from the remainder of the lease, if the tenant, upon 30 days notice to the owner, requests to be released.<br /> <br />

If the owner refuses to consent to an assignment and does have reasonable grounds for withholding
consent, the tenant cannot assign and the owner is not required to release the tenant from the lease. For additional information see, DHCR Fact Sheet #7.
</div>
</div>

<div class="page">
  <div style= "color: #9B9B9B" class="text-center">
    Page 7 of 10
    </div>

<div class="mt-3">
<strong> 10.  WHEN AN OWNER MAY REFUSE TO RENEW A LEASE  </strong> <br />
As long as a tenant pays the lawful rent to which the owner is entitled, the tenant, except for the specific grounds in the Rent Stabilization Law and Rent Stabilization Code, is entitled to remain in the apartment. An owner may not harass a tenant by engaging in an intentional course of conduct intended to make the tenant move from his/her apartment. <br /> <br />
Below are listed some but not all grounds for eviction: <br /> <br />
<strong> Without DHCR consent, </strong> the owner may refuse to renew a lease and bring an eviction action in Civil Court at the expiration of the lease on any of the following grounds: <br />
&emsp;  (A) the tenant refuses to sign a proper renewal lease offered by the owner; <br />
&emsp;  (B) the owner seeks the apartment in good faith for personal use or for the personal use of members of the owner’s immediate family;<br />
&emsp;  (C) the building is owned by a hospital, convent, monastery, asylum, public institution, college, school, dormitory or any institution operated exclusively for charitable or educational purposes and the institution requires the apartment for residential or nonresidential use pursuant to its charitable or educational purposes: or<br />
&emsp;  (D) the tenant does not occupy the apartment as his or her primary residence. The owner must notify the tenant in writing at least 90 and not more than 150 days prior to the expiration of the lease term of the owner’s intention not to renew the lease.<br />
With DHCR consent, the owner may refuse to renew a lease upon any of the following grounds:<br /> <br />
&emsp;  (A) the owner seeks in good faith to recover possession of the apartment for the purpose of demolishing<br />
&emsp;  (B) the owner requires the apartment or the land for the owner’s own use in connection with a business which the owner owns and operates.<br />
A tenant will be served with a copy of the owner’s application and has a right to object. If the owner’s application is granted, the owner may bring an eviction action in Civil Court.<br />
</div>

<div class="mt-3">
<strong>11. <a class="annotation-link" data-toggle="modal" data-target="#eviction">EVICTION WHILE THE LEASE IS IN EFFECT</a></strong> <br />

The owner may bring an action in Civil Court to evict a tenant during the term of the lease for the grounds stated in the Rent Stabilization Law and Rent Stabilization Code.<br /> <br />

Below are listed some but not all grounds for eviction:<br />

&emsp; (A) does not pay rent; <br />
&emsp; (B) is violating a substantial obligation of the tenancy;<br />
&emsp; (C) is committing or permitting a nuisance;<br />
&emsp; (D) is illegally using or occupying the apartment;<br />
&emsp; (E) has unreasonably refused the owner access to the apartment for the purpose of making necessary repairs or improvements required by law or authorized by DHCR, or for the purpose of inspection or showing. The tenant must be given at least 5 days notice of any such inspection or showing, to be arranged at the mutual convenience of the tenant and owner, so to enable the tenant to be present at the inspection or showing. A tenant cannot be required to permit access for inspection or showing if such requirement would be contrary to the lease; or <br />
&emsp; (F) is occupying an apartment located in a cooperative or condominium pursuant to an Eviction Plan. (See subdivision (D) of provision 7 of this Rider, “Renewal Leases”.) A non-purchasing tenant pursuant to a Non-Eviction Plan may not be evicted, except on the grounds set forth in (A) - (E) above. <br /> <br />

Tenants are cautioned that causing violations of health, safety, or sanitation standards of housing maintenance laws, or permitting such violations by a member of the family or of the household or by a guest, may be the basis for a court action by the owner.
</div>

<div class="mt-3">
<strong>12. <a class="annotation-link" data-toggle="modal" data-target="#co-op-and-condo-conversion">COOPERATIVE AND CONDOMINIUM CONVERSION</a></strong> <br />

Tenants who do not purchase their apartments under a Non-Eviction Conversion Plan continue to be protected by Rent Stabilization. Conversions are regulated by the New York State Attorney General. Any cooperative or condominium conversion plan accepted for  ling by the New York State Attorney General’s Office will include speci c information about tenant rights and protections. An informational booklet about the general subject of conversion is available from the New York State Attorney General’s Office. <br /> <br />

A Senior Citizen or a Disabled Person in a building which is being converted to cooperative or condominium ownership pursuant to an Eviction Plan is eligible for exemption from the requirement to purchase his/her apartment to remain in occupancy. This exemption is available to Senior Citizens, or to Disabled Persons with impairments expected to be permanent, which prevent them from engaging in any substantial employment. A Conversion Plan accepted for  ling by the New York State Attorney General’s Office must contain specific information regarding this exemption.
</div>

<div class="mt-3">
<strong>13. <a class="annotation-link" data-toggle="modal" data-target="#scrie-and-drie">SENIOR CITIZENS AND DISABILITY RENT INCREASE EXEMPTION PROGRAM</a></strong> <br />

Tenants or their spouses who are 62 years of age, or older, or are persons with a disability, and whose household income level does not exceed the established income level may qualify for an exemption from Guidelines rent increases, hardship rent increases, major capital improvement rent increases and rent reductions for DHCR approved electrical sub-metering conversions and High-Rent High-Income deregulation. This exemption will only be for a portion of the increase which causes the tenant’s rent to exceed one-third of the “net” household income, and is not available for increases based on new services or equipment within the apartment. Questions concerning the Senior Citizen Rent Increase Exemption (SCRIE) program and the Disability Rent Increase Exemption (DRIE) program can be addressed to the New York City Department of Finance.
</div>

</div>

<div class="page">
  <div style= "color: #9B9B9B" class="text-center">
    Page 8 of 10
    </div>
<div class="mt-3">
When a senior citizen or person with a disability is granted a rent increase exemption, the owner may obtain a real estate tax credit from New York City equal to the amount of the tenant’s exemption. Notwithstanding any of the above, a senior citizen or person with a disability who receives a rent increase exemption is still required to pay a full month’s rent as a security deposit. For additional information see DHCR Fact Sheet # 20 and # 21.
</div>

<div class="mt-3">
<strong>14. SPECIAL CASES AND EXCEPTIONS</strong><br />

Some special rules relating to stabilized rents and required services may apply to newly constructed buildings which receive tax abatement or exemption, and to buildings rehabilitated under certain New York City, New York State, or federal  nancing or mortgage insurance programs. The rules mentioned in this Rider do not necessarily apply to rent stabilized apartments located in hotels. A separate Hotel Rights Notice informing permanent hotel tenants and owners of their basic rights and responsibilities under the Rent Stabilization Law is available from DHCR.
</div>

<div class="mt-3">
<strong>15. <a class="annotation-link" data-toggle="modal" data-target="#high-income-rent-deregulation">HIGH INCOME RENT DEREGULATION</a></strong><br />

The Rent Act of 2015 modified the Deregulation Rent Threshold (DRT) for both High-Rent Vacancy Deregulation and High-Rent High-Income Deregulation. The DRT for both kinds of deregulation was increased to $2,700 and will be increased on January 1, 2016 and each January 1st thereafter by the one year renewal lease guideline percentage issued the prior year by the rent guidelines board for the locality. <br /> <br />

Upon the issuance of an Order by DHCR, apartments which: (1) are occupied by persons who have a total annual income in excess of $200,000 per annum for each of the two preceding calendar years and (2) have a legal regulated rent at the DRT, shall no longer be subject to rent regulation (“High Income Rent Deregulation”). The Rent Stabilization Law permits an owner to  le a Petition for High Income Rent Deregulation on an annual basis. As part of the process, the tenant will be required to identify all persons who occupy the apartment as their primary residence on other than a temporary basis, excluding bona  de employees of the tenant(s) and sub-tenants, and certify whether the total annual income was in excess of $200,000 in each of the two preceding calendar years. If the tenant fails to provide the requested information to DHCR, an order of deregulation will be issued. If the tenant provides the requested information and certi es that the total annual income was not in excess of $200,000, the NYS Department of Taxation and Finance will review whether the apartment is occupied by persons who have a total annual income in excess of $200,000 in each of the two preceding calendar years. <strong>Owners cannot serve the Income Certi cation Forms and/or Petition for High Income Rent Deregulation on an apartment where the tenant is the recipient of a Senior Citizen Rent Increase Exemption (SCRIE) or a Disability Rent Increase Exemption (DRIE).</strong>
</div>

<div class="mt-3">
<strong>16. <a class="annotation-link" data-toggle="modal" data-target="#high-rent-vacancy-deregulation">HIGH RENT VACANCY DEREGULATION</a></strong><br />

The Rent Act of 2015 modified the <strong> Deregulation Rent Threshold (DRT)</strong> for both High-Rent Vacancy Deregulation and High-Rent High-Income Deregulation. The DRT for both kinds of deregulation was increased to $2,700 and will be increased on January 1, 2016 and each January 1st thereafter by the one year renewal lease guideline percentage issued the prior year by the rent guidelines board for the locality.<br /> <br />

When a tenant moves into a vacant apartment and the rent has lawfully reached the Deregulation Rent Threshold, such apartment qualities for permanent deregulation, and therefore for removal from all rent regulation.<br /> <br />

Pursuant to the Rent Code Amendments of 2014, the  rst tenant of the apartment after it becomes deregulated is required to be served by the owner with a DHCR Notice (HRVD-N). The notice is required to contain the reason for deregulation, the last regulated rent and the calculation of the new rent that quali ed for deregulation. In addition, the owner is required to serve the tenant with a copy of a registration statement  led with DHCR indicating the deregulated status and the last legal regulated rent.
</div>

<div class="mt-3">
<strong>17.   RENEWAL LEASES OFFERED DURING PENDENCY OF HIGH INCOME DEREGULATION PROCEEDINGS</strong><br />

Where a High Income Deregulation Proceeding is pending before DHCR and the owner is required to offer a renewal lease to the tenant, a separate rider may be attached to and served with the Rent Stabilization Law “Renewal Lease Form” (RTP-8). If so, attached and served, it shall become part of and modify the Notice and Renewal Lease. The text of the rider is set forth below and may not be modi ed or altered without approval of DHCR.
</div>

<div class="mt-3">
<strong>NOTICE TO TENANT</strong><br />

Pursuant to Section 5-a of the Emergency Tenant Protection Act, or Section 26-504.3 of the Rent Stabilization Law, the owner has commenced a proceeding before DHCR for deregulation of your apartment by  ling a Petition by Owner for High Income Rent Deregulation on _________________, 20_____.<br />
                                                  (Date) <br /> <br />

That proceeding is now pending before DHCR. If DHCR grants the petition for deregulation, this renewal lease shall be cancelled and shall terminate after 60 days from the date of issuance of an order granting such petition. In the event that you  file Petition for Administrative Review (PAR) the order of deregulation, or if you have already  led such PAR and it is pending before DHCR at the time you receive this Notice, and the PAR is subsequently dismissed or denied, this renewal lease shall be cancelled and shall terminate after 60 days from the issuance by DHCR of an order dismissing or denying the PAR. <br /> <br />

Upon such termination of this renewal lease, the liability of the parties for the further performance of the terms, covenants and conditions of this renewal lease shall immediately cease.
</div>
</div>

<div class="page">
  <div style= "color: #9B9B9B" class="text-center">
    Page 9 of 10
    </div>
<div class="mt-3">
<strong>18. AIR CONDITIONER SURCHARGES</strong><br />

Owners are authorized to collect surcharges from rent stabilized tenants for the use of air conditioners. DHCR issues an annual update to an Operational Bulletin in which the lawful surcharges are established for the year. One surcharge amount is established for tenants in buildings where electricity is included in the rent. Another surcharge is established for tenants who pay for their own electricity. Such surcharges shall not become part of the legal regulated rent. (See Operational Bulletin 84-4 and Fact Sheet # 27).
</div>

<div class="mt-3">
<strong>19.   SURCHARGES FOR TENANT INSTALLED WASHING MACHINES, DRYERS AND DISHWASHERS</strong><br />

Unless a lease provides otherwise, owners are not required to allow tenants to install washing machines, dryers or dishwashers. Where a tenant requests permission from the owner to install such appliance or appliances, whether permanently installed or portable, and the owner consents, the owner may collect a surcharge or surcharges. DHCR issues periodic updates to an Operational Bulletin that sets forth surcharges for washing machines, dryers and dishwashers. One set of surcharges is established for tenants in buildings where electricity is included in the rent. Another set of surcharges is established for tenants who pay their own electricity. Such surcharges shall not become part of the rent. (See Operational Bulletin 2005-1).
</div>

<div class="mt-3">
<strong>20.   PREFERENTIAL RENT</strong><br />

A preferential rent is a rent which an owner agrees to charge that is lower than the legal regulated rent that the owner could lawfully collect. The legal regulated rent is required to be written into the vacancy lease and all subsequent renewal leases. The terms of the lease may affect the owner’s right to terminate a preferential rent. If the lease agreement contains a clause that the preferential rent shall continue for the term of the tenancy, not just the speci c lease term, then the preferential rent cannot be terminated for that tenancy. The preferential rent continues to be the basis for future rent increases. However, if the lease is silent and did not contain a clause that clari ed whether the preferential rent was for the “term of the lease” or “the entire term of the tenancy”, then the owner may terminate the preferential rent at the time of the lease renewal. Ordinarily, the rental history preceding the 4 year period to the  ling of an overcharge complaint will not be examined. However, the Rent Code Amendments of 2014 do provide that when an owner claims that the rent being charged is “preferential”, DHCR will examine the lease and rent history immediately preceding such preferential rent, even if it is before 4 years, to assure that the higher “legal” rent is correctly calculated and lawful. (See Fact Sheet # 40.)<br /> <br />

The Rent Act of 2015 modified the vacancy allowance that an owner can add to the legal regulated rent when the vacating tenant was paying a preferential rent. If a vacating tenant was paying a preferential rent, the vacancy lease rent increase that can be applied to the vacating tenant’s legal rent will be limited to 5% if the last vacancy lease commenced less than two years ago, 10% if less than three years ago, 15% if less than four years ago and 20% if four or more years ago.
</div>

<div class="mt-3">
<strong>21. LANGUAGE ACCESS</strong><br />
Copies of the Rider are available for informational purposes only, in languages required by DHCR’s Language Access Plan and can be viewed at www.nyshcr.org. However, the Rider is required to be offered and executed in English only, at the issuance of a vacancy lease or renewal lease. The DHCR RTP-8 Renewal Lease Form is also required to be offered and executed in English only.<br /> <br />

Copias de la Cláusula están disponibles con  nes informativos en los idiomas requeridos por el Plan de Acceso Lingüístico de la DHCR y se pueden ver en www.nyshcr.org. Sin embargo, se requiere que la Cláusula se ofrezca y ejecute en inglés solamente, en la emisión de un contrato de arrendamiento por desocupación o contrato de renovación de arrendamiento. El Formulario del Contrato de Renovación de Arrendamiento RTP-8 de la DHCR también se debe ofrecer y ejecutar en inglés solamente.<br /> <br />

Kopi Dokiman Siplemantè a disponib pou bay enfòmasyon sèlman, nan lang ki obligatwa dapre Plan Aksè nan Lang DHCR epi ou kapab wè yo sou sitwèb www.nyshcr.org. Men, yo fèt pou bay ak egzekite Dokiman Siplemantè a nan lang Anglè sèlman, lè y ap bay yon nouvo kontra lwaye oswa yon renouvèlman kontra lwaye. Pwopriyetè kayla gen obligasyon tou pou bay ak egzekite Fòm Renouvèlman Kontra Lwaye DHCR RTP-8 nan lang Anglè sèlman.<br /> <br />

Copie della postilla sono disponibili per  nalità esclusivamente informative nelle lingue previste dal Piano di assistenza linguistica (Language Access Plan) del DHCR e sono consultabili sul sito www.nyshcr.org. La postilla, tuttavia, va presentata e resa esecutiva solo in lingua inglese, alla stipula di un contratto di locazione di immobile libero o di rinnovo. Anche il modulo del contratto di rinnovo RTP-8 del DHCR va presentato e perfezionato solo in lingua inglese.<br /> <br />

Копии данного Приложения доступны исключительно в информационных целях на языках, предусмотренных Программой языкового доступа (Language Access Plan) Жилищно-коммунальной администрации на сайте www.nyshcr.org. Однако настоящее Приложение должно быть предложено и подписано исключительно на английском языке при подписании вновь заключенного договора аренды или договора о продлении срока аренды. Форма продления срока аренды RTP-8 Жилищно-коммунальной <br />
</div>
</div>

<div class="page">
  <div style= "color: #9B9B9B" class="text-center">
    Page 10 of 10
    </div>
  <div class="mt-3 text-center">
    <strong>Appendix</strong> <br />
    Some agencies which can provide assistance
  </div>

  <div class="mt-5">
    New York State Division of Housing and Community Renewal (DHCR)<br />
    DHCR is a state agency empowered to administer and enforce the Rent Laws. Tenants can contact DHCR at our website: www.nyshcr.org or by visiting one of our Public Information Of ces listed below for assistance.
  </div>

<div class="row">
  <div class="col">
  <strong>Queens</strong><br />
  92-31 Union Hall Street<br />
  Jamaica, NY 11433<br />

  <strong>Lower Manhattan</strong><br />
  25 Beaver Street<br />
  New York, NY 10004<br />

  <strong>Upper Manhattan</strong><br />
  163 West 125th Street<br />
  New York, NY 10027<br />


  </div>
  <div class="col">
  <strong>Bronx</strong><br />
  2400 Halsey Street<br />
  Bronx, NY 10461<br />

  <strong>Brooklyn</strong><br />
  55 Hanson Place<br />
  Brooklyn, NY 11217
  </div>
</div>

  <div class="mt-5">
  Attorney General of the State of New York  - www.ag.ny.gov<br />
  120 Broadway, New York, NY 10271 <br />

  Consumer Frauds and Protection Bureau<br />

  - investigates and enjoins illegal or fraudulent business practices, including the overcharging of rent and mishandling of rent security deposits by owners.<br />

  Real Estate Financing Bureau<br />

  - administers and enforces the laws governing cooperative and condominium conversions. Investigates complaints from tenants in buildings undergoing cooperative or condominium conversion concerning allegations of improper disclosure, harassment, and misleading information.

  </div>
<div class="mt-5">
  Mayor’s Office for People with Disabilities - www.nyc.gov/mopd <br />
  - 100 Gold Street, 2nd Floor, New York, NY 10038<br />

  New York City Rent Guidelines Board (RGB): - www.housingnyc.com<br />
  51 Chambers Street, Room 202, New York, N.Y. 10007<br />

  - promulgates annual percentage of rent increases for rent stabilized apartments and provides information on guidelines orders.<br />

  Copies of New York State and New York City rent laws are available in the business section of some public libraries or NYS.gov. A person should call or write to a public library to determine the exact library which has such legal material.<br />

  DHCR has approved this form and font size as in compliance with RSC section 2522.5(c).
</div>


</div>
