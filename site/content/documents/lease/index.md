---
title: Lease
description: Standard Lease
date: 2015-01-01
publishdate: 2015-01-01
menu: main
name: “Standard Lease”
weight: 1
slug: lease
page:
  - section:
    - title: Title text
    - text: >-
        Add the text in here
---

<div class="page">
  <div style= "color: #9B9B9B" class="text-center">
    Page 1 of 7
    </div>
<div class="mt-3">
</div>
  <div class="mt-3 text-center">
  <strong><a class="annotation-link" data-toggle="modal" data-target="#rent-stabilized-rider">ATTACHED RIDER SETS FORTH RIGHTS AND OBLIGATIONS OF TENANTS AND LANDLORDS UNDER THE RENT STABILIZATION LAW.</a> (LOS DERECHOS Y RESPONSABILIDADES DE INQUILINOS Y CASEROS ESTÁN DISPONIBLE EN ESPAÑOL).</strong>
  </div>

  <div class="border p-3 p-sm-4 p-xl-5 mt-3">
  <strong><a class="annotation-link" data-toggle="modal" data-target="#what-to-expect-in-a-lease">STANDARD FORM OF APARTMENT LEASE THE REAL ESTATE BOARD OF NEW YORK, INC.</a></strong> Copyright 1988. All Rights Reserved. Reproduction in whole or in part prohibited.
  </div>

  <div class="mt-3">
  <strong>PREAMBLE:</strong> This lease contains the agreements between You and Owner concerning <a class="annotation-link" data-toggle="modal" data-target="#personal-protections">Your rights and obligations and the rights and obligations of Owner. </a> You and Owner have other rights and obligations which are set forth in government laws and regulations.
  &emsp;
  You should read this Lease and all of its attached parts carefully. If You have any questions, or if You do not understand any words or statements, get clarification. <a class="annotation-link" data-toggle="modal" data-target="#rights-to-representation">Once You and Owner sign this Lease You and Owner will be presumed to have read it and understood it.</a> You and Owner admit that all agreements between You and Owner have been written into this Lease. You understand that any agreements made before or after this Lease was signed and not written into it will not be enforceable.
  </div>

  <div class="mt-3">
  THIS LEASE is made on (date/month/year): <span class="line-field full"> </span><br/> between Owner,<span class="line-field half"></span> whose address is:<span class="line-field full"></span><br/> and You, the Tenant, <span class="line-field half"></span> whose address is: <span class="line-field full"></span>.
  </div>

  <div class="mt-3">
  <strong>1 . APARTMENT AND USE</strong>
  &emsp; Owner agrees to lease to You Apartment ____________ on the____________loor in the Building at ____________Borough of ____________, City and State of New York. You shall use the Apartment for living purposes only. The Apartment may be occupied by the tenant or tenants named above and by the immedite family of the tenant or tenatnts and by occupants as definied in and only in accordance with the Real Property Law §235-f.
  </div>

  <div class="mt-3">
  <strong><a class="annotation-link" data-toggle="modal" data-target="#lease-renewal">2. LENGTH OF LEASE</a></strong>
  &emsp; The term (that means the length) of this Lease is ____________years, ____________months ____________days, beginning on ____________ and ending on ____________. If you do not do everything You agree to do in this Lease, Owner may have the right to end it before the above date. If Owner does not do everything that owner agrees to do in this Lease, You may have the right to end the Lease before ending date.
  </div>

  <div class="mt-3">
  <strong><a class="annotation-link" data-toggle="modal" data-target="#rent-stabilized-unit">3. RENT</a></strong>
  &emsp; <a class="annotation-link" data-toggle="modal" data-target="#rent-charges-and-rent-increases">Your monthly rent </a> for the Apartment is <a class="annotation-link" data-toggle="modal" data-target="#rent-history"><span class="fake-underline">$____________</span></a> until adjusted pursuant to Article 4 below. You must pay Owner the rent, in advance, on the first day of each month either at Owner’s office or at another place that Owner may inform You of by written notice. <a class="annotation-link" data-toggle="modal" data-target="#preferential-rents">You must pay the first month’s rent to Owner when You sign this Lease if the lease begins on the first day of the month. </a> If the Lease begins after the first day of the month, You must pay when you sign this lease (1) the part of the rent from the beginning date of this Lease until the last day of the month and (2) the full rent for the next full calendar month. If this Lease is a Renewal Lease, the rent for the first month of this Lease need not be paid until the first day of the month when the renewal term begins.
  </div>

  <div class="mt-3">
  <strong><a class="annotation-link" data-toggle="modal" data-target="#reasons-for-rent-destablization">4. RENT ADJUSTMENTS</a></strong>
  &emsp; If this Lease is for a Rent Stabilized apartment, the rent herein shall be adjusted up or down during the Lease term, including retroactively, to conform to the Rent Guidelines. Where Owner, upon application to the State Division of Housing and Community Renewal (“authorized agency”) is found to be entitled to an increase in rent or other relief, You and Owner agree: a. to be bound by such determination; b. where the authorized agency has granted an increase in rent, <a class="annotation-link" data-toggle="modal" data-target="#rent-increase-exemptions">You shall pay such increase in the manner set forth by the authorized agency;</a> c. except that in the event that an order is issued increasing the stabilization rent because of Owner hardship, You may, within (30) days of your receipt of a copy of the order, cancel your lease on sixty (60) days written notice to Owner. During said period You may continue in occupancy at no increase in rent. days written notice to Owner. During said period You may continue in occupancy at no increase in rent.
  </div>

  <div class="mt-3">
  <strong><a class="annotation-link" data-toggle="modal" data-target="#security-deposits">5. SECURITY DEPOSIT</a></strong>
  &emsp; You are required to give Owner the sum of $_______________________when You sign this Lease as a security deposit, which is called in law a trust. Owner will deposit this security in ______________ bank at _______________. If the Building contains six or more apartments, the bank account will earn interest. If You carry out all of your agreements in this Lease, at the end ofeach calendar year Owner or the bank will pay to Owner 1% interest on the deposit for administrative costs and to You all other interest earned on the security deposit.
  &emsp; If You carry out all of your agreements in this Lease and if You move out of the Apartment and return it to Owner in the same condition it was in when You first occupied it, except for ordinary wear and tear or damage caused by fire or other casualty, Owner will return to You the full amount of your security deposit and interest to which You are entitled within 60 days after this Lease ends. However, if You do not carry out all your agreements in this Lease, Owner may keep all or part of your security deposit and any interest which has not yet been paid to You necessary to pay Owner for any losses incurred, including missed payments.
  &emsp; If Owner sells or leases the building, Owner will turn over your security, with interest, either to You or to the person buying or leasing (lessee) the building within 5 days after the sale or lease. Owner will then notify You, by registered or certified mail, of the name and address of the person or company to whom the deposit has been turned over. In such case, Owner will have no further responsibility to You for the security deposit. The new owner or lessee will become responsible to You for the security deposit.
  </div>

  <div class="mt-3">
  <strong>6. IF YOU ARE UNABLE TO MOVE IN</strong>
  &emsp; A situation could arise which might prevent Owner from letting You move into the Apartment on the beginning date set in this Lease. If this happens for reasons beyond Owner’s reasonable control, Owner will not be responsible for Your damages or expenses, and this Lease will remain in effect. However, in such case, this Lease will start on the date when You can move in, and the ending date in Article 2 will be changed to a date reflecting the full term of years set forth in Article 2. You will not have to pay rent until the move-in date Owner gives You by written notice, or the date You move in, whichever is earlier. If Owner does not give You notice that the move-in date is within 30 days after the beginning date of the term of this Lease as stated in Article 2, You may tell Owner in writing, that Owner has 15 additional days to let You move in, or else the Lease will end. If Owner does not allow You to move in within those additional 15 days, then the Lease is ended. Any money paid by You on account of this Lease will then be refunded promptly by Owner.
  </div>
</div>

<div class="page">
  <div style= "color: #9B9B9B" class="text-center">
  Page 2 of 7
  </div>
  <div class="mt-3">
  <strong>7. CAPTIONS </strong> <br />
  &emsp; In any dispute arising under this Lease, in the event of a conflict between the text and a caption, the text controls.
  </div>

  <div class="mt-3">
   <strong><a class="annotation-link" data-toggle="modal" data-target="#warranty-of-habitability">8 . WARRANTY OF HABITABILITY</a></strong>
  &emsp; A. All of the sections of this Lease are subject to the provisions of the Warranty of Habitability Law in the form it may have from time to time during this Lease. Nothing in this Lease can be interpreted to mean that You have given up any of your rights under that law. Under that law, Owner agrees that the Apartment and the Building are fit for human habitation and that there will be no conditions which will be detrimental to life, health or safety.
  &emsp; B. You will do nothing to interfere or make more difficult Owner’s efforts to provide You and all other occupants of the Building with the required facilities and services. Any condition caused by your misconduct or the misconduct of anyone under your direction or control shall not be a breach by Owner.
  </div>

  <div class="mt-3">
  <strong>9. CARE OF YOUR APARTMENT-END OF LEASE-MOVING OUT</strong>
  &emsp; A. You will take good care of the apartment and will not permit or do any damage to it, except for damage which occurs through ordinary wear and tear. You will move out on or before the ending date of this lease and leave the Apartment in good order and in the same condition as it was when You first occupied it, except for ordinary wear and tear and damage caused by fire or other casualty.</br>
 &emsp; B. When this Lease ends, You must remove all of your movable property. You must also remove at your own expense, any wall covering, bookcases, cabinets, mirrors, painted murals or any other installation or attachment You may have installed in the Apartment, even if it was done with Owner’s consent. You must restore and repair to its original condition those portions of the Apartment affected by those installations and removals. You have not moved out until all persons, furniture and other property of yours is also out of the Apartment. If your property remains in the Apartment after the Lease ends, Owner may either treat You as still in occupancy and charge You for use, or may consider that You have given up the Apartment and any property remaining in the Apartment. In this event, Owner may either discard the property or store it at your expense. You agree to pay Owner for all costs and expenses incurred in removing such property. The provisions of this article will continue to be in effect after the end of this Lease.
  </div>

  <div class="mt-3">
  <strong>10. CHANGES AND ALTERATIONS TO APARTMENT</strong>
  &emsp; You cannot build in, add to, change or alter, the Apartment in any way, including wallpapering, painting, repainting, or other decorating, without getting Owner's written consent before You do anything. Without Owner’s prior written consent, You cannot install or use in the Apartment any of the following: dishwasher machines, clothes washing or drying machines, electric stoves, garbage disposal units, heating, ventilating or air conditioning units or any other electrical equipment which, in Owner’s reasonable opinion, will overload the existing wiring installation in the Building or interfere with the use of such electrical wiring facilities by other tenants of the Building. Also, You cannot place in the Apartment water-filled furniture.
  </div>

  <div class="mt-3">
  <strong>11. YOUR DUTY TO OBEY AND COMPLY WITH LAWS, REGULATIONS AND LEASE RULES</strong>
  &emsp; A. <strong>Government Laws and Orders.</strong> You will obey and comply (1) with all present and future city, state and federal laws and regulations, including the Rent Stabilization Code and Law, which affect the Building or the Apartment, and (2) with all orders and regulations of Insurance Rating Organizations which affect the Apartment and the Building. You will not allow any windows in the Apartment to be cleaned from the outside, unless the equipment and safety devices required by law are used.
  &emsp; B. <strong> Owner’s Rules Affecting You.</strong> You will obey all Owner’s rules listed in this Lease and all future reasonable rules of Owner or Owner’s agent. Notice of all additional rules shall be delivered to You in writing or posted in the lobby or other public place in the building, Owner shall not be responsible to You for not enforcing any rules, regulations or provisions of another tenant’s lease except to the extent required by law.
  &emsp; C. <strong>Your Responsibility.</strong> You are responsible for the behavior of yourself, of your immediate family, your servants and people who are visiting You. You will reimburse Owner as additional rent upon demand for the cost of all losses, damages, fines and reasonable legal expenses incurred by Owner because You, members of your immediate family, servants or people visiting You have not obeyed government laws and orders of the agreements or rules of this Lease.
  </div>

  <div class="mt-3">
  <strong>12. OBJECTIONABLE CONDUCT</strong>
  &emsp; As a tenant in the Building, You will not engage in objectionable conduct. Objectionable conduct means behavior which makes or will make the Apartment or the Building less fit to live in for You or other occupants. It also means anything which interferes with the right of others to properly and peacefully enjoy their Apartments, or causes conditions that are dangerous, hazardous, unsanitary and detrimental to other tenants in the Building. Objectionable conduct by You gives Owner the right to end this Lease.
  </div>

  <div class="mt-3">
  <strong>13. SERVICES AND FACILITIES</strong>
   &emsp; A. <strong>Required Services.</strong> Owner will provide cold and <a class="annotation-link" data-toggle="modal" data-target="#hot-water">hot water</a> and <a class="annotation-link" data-toggle="modal" data-target="#heating-season">heat</a> as required by law, repairs to the Apartment, as required by law, elevator service if the Building has elevator equipment, and the utilities, if any, included in the rent, as set forth in sub-paragraph B. You are not entitled to any rent reduction because of a stoppage or reduction of any of the above services unless it is provided by law.
  &emsp; B. The following utilities are included in the rent: <br/><span class="line-field full"></span>
  &emsp; C. <strong>Electricity and Other Utilities.</strong> If Owner provides electricity or gas and the charge is included in the rent on Page 1, or if You buy electricity or gas from Owner for a separate (submetered) charge, <a class="annotation-link" data-toggle="modal" data-target="#service-disruption">your obligations are described in the Rider attached to this Lease.</a> If electricity or gas is not included in the rent or is not charged separately by Owner, You must arrange for this service directly with the utility company. You must also pay directly for telephone service if it is not included in the rent.
  &emsp; D. <strong>Appliances.</strong> Appliances supplied by Owner in the Apartment are for your use. They will be maintained and repaired or replaced by Owner, but if repairs or replacement are made necessary because of your negligence or misuse, You will pay Owner for the cost of such repair or replacement as additional rent.
  &emsp; E. <strong>Elevator Service.</strong> If the elevator is the kind that requires an employee of Owner to operate it, Owner may end this service without reducing the rent if: (1) Owner gives You 10 days notice that this service will end; and (2) within a reasonable time after the end of this 10-day notice, Owner begins to substitute an automatic control type of elevator and proceeds diligently with its installation.
  </div>

  <div class="mt-3">
   <strong>14. INABILITY TO PROVIDE SERVICES</strong>
  &emsp; Because of a strike, labor trouble, national emergency, repairs, or any other cause beyond Owner’s reasonable control, Owner may not be able to provide or may be delayed in providing any services or in making any repairs to the Building. In any of these events, any rights You may have against Owner are only those rights which are allowed by laws in effect when the reduction in service occurs.
  </div>
  </div>

<div class="page">
    <div style= "color: #9B9B9B" class="text-center">
      Page 3 of 7
      </div>

  <div class="mt-3">
  <strong><a class="annotation-link" data-toggle="modal" data-target="#a-right-to-privacy">15. ENTRY TO APARTMENT</a></strong>
  &emsp;During reasonable hours and with reasonable notice, except in emergencies, Owner may enter the Apartment for the following reasons:
  &emsp;A. To erect, use and maintain pipes and conduits in and through the walls and ceilings of the Apartment; to inspect the Apartment and to make any necessary repairs or changes Owner decides are necessary. Your rent will not be reduced because of any of this work, unless required by Law.
  &emsp;B. To show the Apartment to persons who may wish to become owners or lessees of the entire Building or may be interested in lending money to Owner;
  &emsp;C. For four months before the end of the Lease, to show the Apartment to persons who wish to rent it;
  &emsp;D. If during the last month of the Lease You have moved out and removed all or almost all of your property from the Apartment, Owner may enter to make changes, repairs, or redecorations. Your rent will not be reduced for that month and this Lease will not be ended by Owner’s entry.
  &emsp;E. If at any time You are not personally present to permit Owner or Owner’s representative to enter the Apartment and entry is necessary or allowed by law or under this lease, Owner or Owner’s representatives may nevertheless enter the Apartment. Owner may enter by force in an emergency. Owner will not be responsible to You, unless during this entry, Owner or Owner's representative is negligent or misuses your property.
  </div>

  <div class="mt-3">
  <strong><a class="annotation-link" data-toggle="modal" data-target="#apartment-sharing">16. ASSIGNING; SUBLETTING; ABANDONMENT</a></strong>
  &emsp; (A) <a class="annotation-link" data-toggle="modal" data-target="#assigning-a-lease">Assigning</a> and <a class="annotation-link" data-toggle="modal" data-target="#subletting">Subletting</a>. You cannot assign this Lease or sublet the Apartment without Owner’s advance written consent in each instance to a request made by You in the manner required by Real Property Law §226.b. and in accordance with the provisions of the Rent Stabilization Code and Law, relating to subletting. Owner may refuse to consent to a lease assignment for any reason or no reason, but if Owner unreasonably refuses to consent to request for a Lease assignment properly made, at your request in writing, Owner will end this Lease effective as of thirty days after your request. The first and every other time you wish to sublet the Apartment, You must get the written consent of Owner unless Owner unreasonably withholds consent following your request to sublet in the manner provided by Real Property Law §226.b. Owner may impose a reasonable credit check fee on You in connection with an application to assign or sublet. If You fail to pay your rent Owner may collect rent from subtenant or occupant without releasing You from the Lease. Owner will credit the amount collected against the rent due from You. However,Owner’s acceptance of such rent does not change the status of the subtenant or occupant to that of direct tenant of Owner and does not release You from this Lease.
  &emsp; (B) Abandonment. If You move out of the Apartment (abandonment) before the end of this Lease without the consent of Owner, this Lease will not be ended (except as provided by law following Owner’s unreasonable refusal to consent to an assignment or subletting requested by You.) You will remain responsible for each monthly payment of rent as it becomes due until the end of this Lease. In case of abandonment, your responsibility for rent will end only if Owner chooses to end this Lease for default as provided in Article 17.
  </div>

  <div class="mt-3">
  <strong>17.DEFAULT</strong>
  &emsp; (1) You default under the Lease if You act in any of the following ways:
&emsp; &emsp; &emsp; a). You fail to carry out any agreement or provision of this Lease;
&emsp; &emsp; &emsp; b). You or another occupant of the Apartment behaves in an objectionable manner;
&emsp; &emsp; &emsp; c). You do not take possession or move into the Apartment 15 days after the beginning of this Lease;
&emsp; &emsp; &emsp; d). You and other legal occupants of the Apartment move out permanently before this Lease ends;

If You do default in any one of these ways, other than a default in the agreement to pay rent, Owner may serve You with a written notice to stop or correct the specified default within 10 days. You must then either stop or correct the default within 10 days, or, if You need more than 10 days, You must begin to correct the default within 10 days and continue to do all that is necessary to correct the default as soon as possible.
&emsp; (2) If You do not stop or begin to correct a default within 10 days, Owner may give You a second written notice that this Lease will end six days after the date the second written notice is sent to You, At the end of the 6-day period, this Lease will end, You then must move out of the Apartment. Even though this Lease ends, You will remain liable to Owner for unpaid rent up to the end of this Lease, the value of your occupancy, if any, after the Lease ends, and damages caused to Owner after that time as stated in Article 18.
&emsp; (3) If You do not pay your rent when this Lease requires after a personal demand for rent has been made, or within three days after a statutory written demand for rent has been made, or if the Lease ends, Owner may do the following: (a) enter the Apartment and retake possession of it if You have moved out; or (b) go to court and ask that You and all other occupants in the Apartment be compelled to move out.

Once this Lease has been ended, whether because of default or otherwise, You give up any right You might otherwise have to reinstate or renew the Lease.
  </div>

  <div class="mt-3">
  <strong>18. REMEDIES OF OWNER AND YOUR LIABILITY</strong>
If this Lease is ended by Owner because of your default, the following are the rights and obligations of You and Owner:
  &emsp; &emsp; &emsp; a). You must pay your rent until this Lease has ended. Thereafter, You must pay an equal amount for what the law calls “use and occupancy” until You actually move out.
  &emsp; &emsp; &emsp; b). Once You are out, Owner may re-rent the Apartment or any portion of it for a period of time which may end before or after the ending date of this Lease. Owner may re-rent to a new tenant at a lesser rent or may charge a higher rent than the rent in this Lease.
  &emsp; &emsp; &emsp; c). Whether the Apartment is re-rented or not, You must pay to Owner as damages:
  &emsp; &emsp; &emsp; &emsp; &emsp; (1) the difference between the rent in this Lease and the amount, if any, of the rents collected in any later lease or leases of the Apartment for what would have been the remaining period of this Lease; and
 &emsp; &emsp; &emsp; &emsp; &emsp; (2) Owner's expenses for advertisements, broker’s fees and the cost of putting the Apartment in good condition for re-rental; and
 &emsp; &emsp; &emsp; &emsp; &emsp; (3) Owner’s expenses for attorney’s fees
  &emsp; &emsp; &emsp; d). You shall pay all damages due in monthly installments on the rent day established in this Lease. Any legal action brought to collect one or more monthly installments of damages shall not prejudice in any way Owner’s right to collect the damages for a later month by a similar action. If the rent collected by Owner from a subsequent tenant of the Apartment is more than the unpaid rent and damages which You owe Owner, You cannot receive the difference. Owner’s failure to re-rent to another tenant will not release or change your liability for damages, unless the failure is due to Owner’s deliberate inaction.
  </div>

<div class="mt-3">
<strong>19. ADDITIONAL OWNER REMEDIES</strong></br>
&emsp;
If You do not do everything You have agreed to do, or if You do anything which shows that You intend not to do what You have agreed to do, Owner has the right to ask a court to make You carry out your agreement or to give the Owner such other relief as the Court can provide. This is in addition to the remedies in Article 17 and 18 of this lease.
</div>

<div class="mt-3">
<strong>20. FEES AND EXPENSES</strong></br>
&emsp;
<strong>A. Owner’s Right.</strong> You must reimburse Owner for any of the following fees and expenses incurred by Owner: </br>
&emsp; &emsp;
(1) Making any repairs to the Apartment or the Building which result from misuse or negligence by You or persons who live with You, visit You, or work for You; </br>
*** This may be deleted.
</div>
</div>

<div class="page">
  <div style= "color: #9B9B9B" class="text-center">
    Page 4 of 7
    </div>
<div class="mt-3">
&emsp; &emsp;
(2) Repairing or replacing any appliance damaged by Your misuse or negligence;</br>
&emsp; &emsp;
(3) Correcting any violations of city, state or federal laws or orders and regulations of insurance rating organizations concerning the Apartment or the Building which You or persons who live with You, visit You, or work for You have caused;</br>
&emsp; &emsp;
(4) Preparing the Apartment for the next tenant if You move out of your Apartment before the Lease</br>
ending date;</br>
&emsp; &emsp;
(5) Any legal fees and disbursements for legal actions or proceedings brought by Owner against You because of
a Lease default by You or for defending lawsuits brought against Owner because of your actions;</br>
&emsp; &emsp;
(6) Removing all of your property after this Lease is ended;</br>
&emsp; &emsp;
(7) All other fees and expenses incurred by Owner because of your failure to obey any other provisions
and agreements of this Lease;</br>
These fees and expenses shall be paid by You to Owner as additional rent within 30 days after You receive
Owner’s bill or statement. If this Lease has ended when these fees and expenses are incurred, You will still be liable to Owner for the same amount as damages.</br>
&emsp;
<strong>B. Tenant’s Right.</strong> Owner agrees that unless sub-paragraph 5 of this Article 20 has been stricken out of this Lease
You have the right to collect reasonable legal fees and expenses incurred in a successful defense by You of a lawsuit brought by Owner against You or brought by You against Owner to the extent provided by Real Property Law, section 234.
</div>

<div class="mt-3">
<strong><a class="annotation-link" data-toggle="modal" data-target="#personal-injury-or-property-damage">21. PROPERTY LOSS, DAMAGES OR INCONVENIENCE</a></strong><br />
&emsp;
Unless caused by the negligence or misconduct of Owner or Owner’s agents or employees, Owner or Owner’s agents and employees are not responsible to You for any of the following: (1) any loss of or damage to You or your property in the Apartment or the Building due to any accidental or intentional cause, even a theft or another crime committed in the Apartment or elsewhere in the Building; (2) any loss of or damage to your property delivered to any employee of the Building (i.e., doorman, superintendent, etc.); or (3) any damage or inconvenience caused to You by actions, negligence or violations of a Lease by any other tenant or person in the Building except to the extent required by law. <br />
&emsp;
Owner will not be liable for any temporary interference with light, ventilation, or view caused by construction by or in behalf of Owner. Owner will not be liable for any such interference on a permanent basis caused by construction on any parcel of land not owned by Owner. Also, Owner will not be liable to You for such interference caused by the permanent closing, darkening or blocking up of windows, if such action is required by law. None of the foregoing events will cause a suspension or reduction of the rent or allow You to cancel the Lease.
</div>
<div class="mt-3">

<strong>22. FIRE OR CASUALTY</strong><br />
&emsp;
A. If the Apartment becomes unusable, in part or totally, because of fire, accident or other casualty, this Lease will continue unless ended by Owner under C below or by You under D below. But the rent will be reduced immediately. This reduction will be based upon the part of the Apartment which is unusable.<br />
&emsp;
B. Owner will repair and restore the Apartment, unless Owner decides to take actions described in paragraph C below.<br />
&emsp;
C. After a fire, accident or other casualty in the Building, Owner may decide to tear down the Building or to substantially rebuild it. In such case, Owner need not restore the Apartment but may end this Lease. Owner may do this even if the Apartment has not been damaged, by giving You written notice of this decision within 30 days after the date when the damage occurred. If the Apartment is usable when Owner gives You such notice, this Lease will end 60 days from the last day of the calendar month in which You were given the notice.<br />
&emsp;
D. If the Apartment is completely unusable because of fire, accident or other casualty and it is not repaired in 30 days, You may give Owner written notice that You end the Lease. If You give that notice, this Lease is considered ended on the day that the fire, accident or casualty occurred. Owner will refund your security deposit and the pro-rata portion of rents paid for the month in which the casualty happened.<br />
&emsp;
E. Unless prohibited by the applicable insurance policies, to the extent that such insurance is collected, You and Owner release and waive all right of recovery against the other or anyone claiming through or under each by way of subrogation.
</div>

<div class="mt-3">
<strong>23. PUBLIC TAKING</strong><br />
&emsp;
The entire building or a part of it can be acquired (condemned) by any government or government agency for
a public or quasi-public use or purpose. If this happens, this Lease shall end on the date the government or agency take title, You shall have no claim against Owner for any damage resulting; You also agree that by signing this Lease, You assign to Owner any claim against the Government or Government agency for the value of the unexpired portion of this Lease.
</div>

<div class="mt-3">
<strong>24. SUBORDINATION CERTIFICATE AND ACKNOWLEDGEMENTS</strong><br />
&emsp;
All leases and mortgages of the Building or of the land on which the Building is located, now in effect or made after this Lease is signed, come ahead of this Lease. In other words, this Lease is “ subject and subordinate to” any existing or future lease or mortgage on the Building or land, including any renewals, consolidations, modifications and replacements of these leases or mortgages. If certain provisions of any of these leases or mortgages come into effect, the holder of such lease or mortgage can end this Lease. If this happens, You agree that You have no claim against Owner or such lease or mortgage holder. If Owner requests, You will sign promptly an acknowledgement of the “subordination” in the form that Owner requires.<br />
&emsp;
You also agree to sign (if accurate) a written acknowledgement to any third party designated by Owner that this Lease is in effect, that Owner is performing Owner’s obligations under this Lease and that you have no present claim against Owner.
</div>

<div class="mt-3">
<strong>25. TENANT’S RIGHT TO LIVE IN AND USE THE APARTMENT</strong><br />
&emsp;
If You pay the rent and any required additional rent on time and You do everything You have agreed to do in this Lease, your tenancy cannot be cut off before the ending date, except as provided for in Articles 22, 23, and 24.
</div>

<div class="mt-3">
<strong>26. BILLS AND NOTICE</strong><br />
&emsp;
A. Notices to You. Any notice from Owner or Owner’s agent or attorney will be considered properly given to
You if it (1) is in writing; (2) is signed by or in the name of Owner or Owner’s agent; and (3 ) is addressed to You at the Apartment and delivered to You personally or sent by registered or certified mail to You at the Apartment. The date of service of any written notice by Owner to you under this agreement is the date of delivery or mailing of such notice.<br />
&emsp;
B. Notices to Owner. If You wish to give a notice to Owner, You must write it and deliver it or send it by registered or certified mail to Owner at the address noted on page 1 of this Lease or at another address of which Owner or Agent has given You written notice.
</div>

<div class="mt-3">
<strong><a class="annotation-link" data-toggle="modal" data-target="#right-to-a-jury-trial">27. GIVING UP RIGHT TO TRIAL BY JURY AND COUNTERCLAIM</a></strong><br />
&emsp;
A. Both You and Owner agree to give up the right to a trial by jury in a court action, proceeding or counter claim on any matters concerning this Lease, the relationship of You and Owner as Tenant and Landlord or your use or occupancy of the Apartment. This agreement to give up the right to a jury trial does not include claims for personal injury or property damage.<br />
&emsp;
B. If Owner begins any court action or proceeding against You which asks that You be compelled to move out, You cannot make a counterclaim unless You are claiming that Owner has not done what Owner is supposed to do about the condition of the Apartment or the Building. <br />
*** This may be deleted.
</div>
</div>

<div class="page">
  <div style= "color: #9B9B9B" class="text-center">
    Page 5 of 7
    </div>
<div class="mt-3">

<strong>28. NO WAIVER OF LEASE PROVISIONS</strong><br />
&emsp;
A. Even if Owner accepts your rent or fails once or more often to take action against You when You have not done what You have agreed to do in this Lease, the failure of Owner to take action or Owner’s acceptance of rent does not prevent Owner from taking action at a later date if You again do not do what You have agreed to do.<br />
&emsp;
B. Only a written agreement between You and Owner can waive any violation of this Lease.<br />
&emsp;
C. If You pay and Owner accepts an amount less than all the rent due, the amount received shall be considered
to be in payment of all or a part of the earliest rent due. It will not be considered an agreement by Owner to accept this lesser amount in full satisfaction of all of the rent due.<br />
&emsp;
D. Any agreement to end this Lease and also to end the rights and obligations of You and Owner must be in writing, signed by You and Owner or Owner’s agent. Even if You give keys to the Apartment and they are accepted by any employee, or agent, or Owner, this Lease is not ended.
</div>

<div class="mt-3">
<strong>29. CONDITION OF THE APARTMENT</strong><br />
&emsp;
When You signed this Lease, You did not rely on anything said by Owner, Owner’s agent or superintendent about the physical condition of the Apartment, the Building or the land on which it is built. You did not rely on any promises as to what would be done, unless what was said or promised is written in this Lease and signed by both You and Owner or found in Owner’s floor plans or brochure shown to You before You signed the Lease. Before signing this Lease, You have inspected the apartment and You accept it in its present condition “as is,” except for any condition which You could not reasonably have seen during your inspection. You agree that Owner has not promised to do any work in the Apartment except as specified in attached “ Work” rider.
</div>

<div class="mt-3">
<strong><a class="annotation-link" data-toggle="modal" data-target="#major-capital-improvement">30. RENT INCREASE FOR MAJOR CAPITAL IMPROVEMENT</a></strong><br />
&emsp;
Owner advises you that an application for increase in stabilized rent on the ground of a building-wide major capital improvement dated ________________ Docket No.____________ is now pending before the State Division of Housing and Community Renewal (Agency). Such application involves the following major capital improvements which are now completed or in progress:
</div>
<div class="mt-3">

<span class="line-field full"></span><br/>
<span class="line-field full"></span><br/>
<span class="line-field full"></span><br/>
<span class="line-field full"></span><br/>

</div>
<div class="mt-3">
You agree that the stabilized rent herein may be increased during the term of this lease by reason of such improvement as of a date and in the amount permitted by an order from the Agency. <a class="annotation-link" data-toggle="modal" data-target="#individual-apartment-improvement"> (IAIs are a bit different) <a />

</div>

<div class="mt-3">
<strong>31. DEFINITIONS</strong><br />
&emsp;
A. Owner: The term “Owner” means the person or organization receiving or entitled to receive rent from You or the Apartment at any particular time other than a rent collector or managing agent of Owner. “Owner”includes the owner of the land or Building, a lessor, or sublessor of the land or Building and a mortgagee in possession. It does not include a former owner, even if the former owner signed this Lease.<br />
&emsp;
B. You: The Term “You” means the person or persons signing this Lease as Tenant and the successors and assigns of the signer. This Lease has established a tenant-landlord relationship between You and Owner.
</div>

<div class="mt-3">
<strong><a class="annotation-link" data-toggle="modal" data-target="#succession-rights">32. SUCCESSOR INTERESTS</a></strong><br />
&emsp;
The agreements in this Lease shall be binding on Owner and You and on those who succeed to the interest of Owner or You by law, by approved assignment or by transfer.<br /> <br />

<strong> Owners Rules - a part of this lease - see page 6 </strong> <br />
TO CONFIRM OUR AGREEMENTS, OWNER AND YOU RESPECTIVELY SIGN THIS LEASE AS OF THE DAY AND YEAR FIRST WRITTEN ON PAGE 1. <br /> <br />
</div>
<div class="row">
  <div class="col">
   Witnesses________________________
  </div>
  <div class="col">
  Owner’s Signature__________________[L.S.]  <br />
  Tenant’s Signature__________________[L.S.] <br />
  Tenant’s Signature__________________[L.S.]
  </div>
</div>

<div class="mt-3">
<strong><a class="annotation-link" data-toggle="modal" data-target="#guarantor">GUARANTY</a></strong><br />
&emsp;
The undersigned Guarantor guarantees to Owner the strict performance of and observance by Tenant of all the agreements, provisions and rules in the attached Lease. Guarantor agrees to waive all notices when Tenant is not paying rent or not observing and complying with all of the provisions of the attached Lease. Guarantor agrees to be equally liable with Tenant so that Owner may sue Guarantor directly without first suing Tenant. The Guarantor further agrees that his guaranty shall remain in full effect even if the Lease is renewed, changed or extended in any way and even if Owner has to make a claim against Guarantor. Owner and Guarantor agree to waive trial by jury in any action, proceeding or counterclaim brought against the other on any matters concerning the attached Lease or the Guaranty. <br />

Dated, New York City <br />
</div>

<div class="row">
  <div class="col">
   Witness________________________
  </div>
  <div class="col">
  Guarantor________________________ <br />
  Address__________________________
  </div>
</div>
</div>

<div class="page">
  <div style= "color: #9B9B9B" class="text-center">
    Page 6 of 7
    </div>
<div class="mt-3">
</div>


<div class="mt-3 text-center"> <strong>ATTACHED RULES WHICH ARE A PART <br />
OF THE LEASE AS PROVIDED BY ARTICLE 11</strong> </div>

<div class="mt-3">

<strong>Public Access Ways</strong><br />
1. &emsp;	(a) Tenants shall not block or leave anything in or on fire escapes, the sidewalks, entrances, driveways, elevators, stairways, or halls. Public access ways shall be used only for entering and leaving the Apartment and the Building. Only those elevators and passageways designated by Owner can be used for deliveries.
(b) Baby carriages, bicycles or other property of Tenants shall not be allowed to stand in the halls, passageways, public areas or courts of the Building.<br />

<strong>Bathroom and Plumbing Fixtures</strong><br />
2. &emsp;	The bathrooms, toilets and wash closets and plumbing fixtures shall only be used for the purposes for which they were designed or built; sweepings, rubbish bags, acids or other substances shall not be placed in them.<br />

<strong>Refuse</strong><br />
3. &emsp;	Carpets, rugs or other articles shall not be hung or shaken out of any window of the Building. Tenants shall not sweep or throw
or permit to be swept or thrown any dirt, garbage or other substances out of the windows or into any of the halls, elevators or elevator shafts. Tenants shall not place any articles outside of the Apartment or outside of the building except in safe containers and only at places chosen by Owner.<br />

<strong>Elevators</strong><br />
4. &emsp;	All non-automatic passenger and service elevators shall be operated only by employees of Owner and must not in any event be interfered with by Tenants. The service elevators, if any, shall be used by servants, messengers and trades people for entering and leaving, and the passenger elevators, if any, shall not be used by them for any purpose . Nurses with children, however, may use the passenger elevators.<br />

<strong>Laundry</strong><br />
5. &emsp; Laundry and drying apparatus, if any, shall be used by Tenants in the manner and at the times that the superintendent or other representative of Owner may direct. Tenants shall not dry or air clothes on the roof.<br />

<strong>Keys and Locks</strong><br />
6. &emsp;  Owner may retain a pass key to the apartment. Tenants may install on the entrance of the Apartment an additional lock of not more than three inches in circumference. Tenants may also install a lock on any window but only in the manner provided by law. Immediately upon making any installation of either type, Tenants shall notify Owner or Owner’s agent and shall give Owner or Owner’s agent a duplicate key. If changes are made to the locks or mechanism installed by Tenants, Tenants must deliver keys to Owner. At the end of this Lease, Tenants must return to Owner all keys either furnished or otherwise obtained. If Tenants lose or fail to return any keys which were furnished to them, Tenants shall pay to Owner the cost of replacing them.<br />

<strong>Noise</strong><br />
7. &emsp; Tenants, their families, guests, employees, or visitors shall not make or permit any disturbing noises in the Apartment or Building or permit anything to be done that will interfere with the rights, comforts or convenience of other tenants. Also, Tenants shall not play a musical instrument or operate or allow to be operated a phonograph, radio or television set so as to disturb or annoy any other occupant of the Building.<br />

<strong>No Projections</strong><br />
8. &emsp; An aerial may not be erected on the roof or outside wall of the Building without the written consent of Owner. Also, awnings or other projections shall not be attached to the outside walls of the Building or to any balcony or terrace.<br />

<strong><a class="annotation-link" data-toggle="modal" data-target="#the-pet-clause">No Pets</a></strong><br />
9. &emsp; Dogs or animals of any kind shall not be kept or harbored in the Apartment, unless in each instance it be expressly permitted in writing by Owner. This consent, if given, can be taken back by Owner at any time for good cause on reasonably given notice. Unless carried or on a leash, a dog shall not be permitted on any passenger elevator or in any public portion of the building. Also, dogs are not permitted on any grass or garden plot under any condition. BECAUSE OF THE HEALTH HAZARD AND POSSIBLE DISTURBANCE OF OTHER TENANTS WHICH ARISE FROM THE UNCONTROLLED PRESENCE OF ANIMALS, ESPECIALLY DOGS, IN THE BUILDING, THE STRICT ADHERENCE TO THE PROVISIONS OF THIS RULE BY EACH TENANT IS A MATERIAL REQUIREMENT OF EACH LEASE. TENANTS’ FAILURE TO OBEY THIS RULE SHALL BE CONSIDERED A SERIOUS VIOLATION OF AN IMPORTANT OBLIGATION BY TENANT UNDER THIS LEASE. OWNER MAY ELECT TO END THIS LEASE BASED UPON THIS VIOLATION.<br />

<strong>Moving</strong><br />
10. &emsp; Tenants can use the elevator to move furniture and possessions only on designated days and hours. Owner shall not be liable for any costs, expenses or damages incurred by Tenants in moving because of delays caused by the unavailability of the elevator. <br />

<strong>Floors</strong><br />
11. &emsp; Apartment floors shall be covered with rugs or carpeting to the extent of at least 80% of the floor area of each room excepting only kitchens, pantries, bathrooms and hallways. The tacking strip for wall-to-wall carpeting will be glued, not nailed to the floor. <br />

<strong>Window Guards</strong><br />
12. &emsp; IT IS A VIOLATION OF LAW TO REFUSE, INTERFERE WITH INSTALLATION, OR REMOVE WINDOW GUARDS WHERE REQUIRED. (SEE ATTACHED WINDOW GUARD RIDER)
</div>
</div>

<div class="page">
  <div style= "color: #9B9B9B" class="text-center">
    Page 7 of 7
    </div>
  <div class="mt-3 text-center">
  <strong>THE REAL ESTATE BOARD OF NEW YORK, Inc. <br />

  The Following forms may be required to be <br />
  attached to the Standard Form of Apartment<br />
  Lease (Copyright 1988):<br />

  Rent Stabilization Lease Rider<br />

  <a class="annotation-link" data-toggle="modal" data-target="#window-guards">Window Guard Rider</a> * </strong>
</div>
<div class="mt-3 text-center">
<strong>Disclosure of Information on Lead-Based Paint and/or Lead Based Paint
Hazards* New York City Lead Paint Notice (English or Spanish Version) * </strong><br />

The Lead Based Paint Disclosure Form has been developed by HUD and EPA to implement federal regulatory requirements which affect all housing built prior to 1978. The requirements take effect September 6, 1996 for buildings of greater than four units, and December 6, 1996 for buildings with one to four units.<br />

<strong>* This form may also be required to be attached to the Standard Form of Apartment Lease (AX- 10/82) and the Loft Lease (Copyright 1999).</strong>
</div>

</div>
