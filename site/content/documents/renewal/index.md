---
title: Renewal
description: Lease Renewal
date: 2015-01-01
publishdate: 2015-01-01
slug: renewal
menu: main
name: “Lease Renewal”
weight: 2
---
<div class="page">
<div style= "color: #9B9B9B" class="text-center">
  Page 1 of 2
  </div>
  <div class="mt-3 text-center">
  <strong><a class="annotation-link" data-toggle="modal" data-target="#explanation-of-renewal-policies">RENEWAL LEASE FORM</a></strong> <br />
  Owners and Tenants should read <strong>INSTRUCTIONS TO OWNER</strong> and <strong>INSTRUCTIONS TO TENANT</strong><br />
  on reverse side before filling out or signing this form<br />
  <strong>THIS IS A NOTICE FOR RENEWAL OF LEASE AND RENEWAL LEASE FORM ISSUED UNDER SECTION 2523.5(a) OF THE RENT STABILIZATION CODE. ALL COPIES OF THIS FORM MUST BE SIGNED BELOW AND RETURNED TO YOUR LANDLORD WITHIN 60 DAYS.</strong>
</div>
<div class="mt-3">
  <a class="annotation-link" data-toggle="modal" data-target="#date-of-the-lease"> Dated </a> :________________________ 20_________
  </div>


<div class="row mt-3">
  <div class="col">
  Tenant's Name(s) and Address:
  <a class="annotation-link" data-toggle="modal" data-target="#tenants-name-and-full-address">_____________________________</a><br />
  <a class="annotation-link" data-toggle="modal" data-target="#tenants-name-and-full-address">_____________________________</a><br />
  <a class="annotation-link" data-toggle="modal" data-target="#tenants-name-and-full-address">_____________________________</a><br />
  <a class="annotation-link" data-toggle="modal" data-target="#tenants-name-and-full-address">_____________________________</a>
 </div>
  <div class="col">
  Owner's /Agent's Name and Address:
  <a class="annotation-link" data-toggle="modal" data-target="#landlords-name-and-full-address">_____________________________</a><br />
  <a class="annotation-link" data-toggle="modal" data-target="#landlords-name-and-full-address">_____________________________</a><br />
  <a class="annotation-link" data-toggle="modal" data-target="#landlords-name-and-full-address">_____________________________</a><br />
  <a class="annotation-link" data-toggle="modal" data-target="#landlords-name-and-full-address">_____________________________</a>
  </div>
</div>

<div class="mt-3">
<a class="annotation-link" data-toggle="modal" data-target="#date-of-expiration-of-current-lease">1.The owner hereby notifies you that your lease<br />
will expire on: _________ / _________ /________</a>
</div>

<div style= "background-color: #9B9B9B";>
<div class="border p-1 p-sm-4 p-xl-1 mt-3 text-center">
  <strong>PART A - OFFER TO TENANT TO RENEW</strong>
</div>
</div>
<div class="mt-3">
2. You may renew this lease, for one or two years, at your option, as follows:
</div>
<div class="table-responsive">
<table class="table table-bordered text-center">

  <tbody>
    <tr>
      <td><strong>Column A</strong>
    Renewal Term</td>
      <td><strong>Column B</strong>
      Legal Rent on Sept.30th Preceding Commencement
      Date of this Renewal Lease</td>
      <td><strong>Column C</strong>
    Guideline % or Minimum $ Amount (If unknown, check box
    and see below)</td>
      <td><strong>Column D</strong>
      Applicable Guideline Supplement, if any</td>
      <td><strong>Column E</strong>
    Lawful Rent Increase, if any, Effective after Sept. 30th</td>
      <td><strong>Column F</strong>
    New Legal Rent (If a lower rent is to be charged, checkbox and see item 5 below)</td>
    </tr>
    <tr>
      <td><strong> <a id="rent-stabilized-rider-anchor" class="annotation-link" data-toggle="modal" data-target="#renewal-terms">1 year <br />
      2 years</a>
      </td>

      <td><strong><a id="rent-stabilized-rider-anchor" class="annotation-link" data-toggle="modal" data-target="#legal-rent-on-current-lease">
      $ ________ <br />
      Same as above
      </a>
      </strong>
      </td>

      <td><strong><a id="rent-stabilized-rider-anchor" class="annotation-link" data-toggle="modal" data-target="#rgb-allowable-rent-increases">(  %)$ ______ <br />
      (  %)$ ______</a></strong>
      </td>

      <td><strong><a id="rent-stabilized-rider-anchor" class="annotation-link" data-toggle="modal" data-target="#other-allowable-increases-iais">$ ______ <br />
      $ ______</a></strong>
      </td>

      <td><strong><a id="rent-stabilized-rider-anchor" class="annotation-link" data-toggle="modal" data-target="#mcis-and-other-lawful-increases">$ ______ <br />
      $ ______</a></strong></td>

      <td><strong><a id="rent-stabilized-rider-anchor" class="annotation-link" data-toggle="modal" data-target="#new-legal-rent">$ ______ <br />
      $ ______</a></strong>
    </tr>
    <tr>

  </tbody>
</table>
</div>

<div class="mt-3">
* If applicable guideline rate is unknown at time offer is made check box in column c and enter current guideline which will be subject to adjustment when rates are ordered.
</div>

<div class="row mt-3">
  <div class="col">
  <strong>3. Security Deposit:</strong><br />
<a id="rent-stabilized-rider-anchor" class="annotation-link" data-toggle="modal" data-target="#current-security-deposit">Current Deposit: $______________</a>

 </div>

  <div class="col">
  Additional Deposit Required - 1 year lease: <a id="rent-stabilized-rider-anchor" class="annotation-link" data-toggle="modal" data-target="#security-deposit-increase">***$________________________***</a> <br/>
  Additional Deposit Required - 2 year lease: <a id="rent-stabilized-rider-anchor" class="annotation-link" data-toggle="modal" data-target="#security-deposit-increase">***$________________________***</a>
  </div>
</div>

<div class="row mt-3">
  <div class="col">
  <strong>4. Specify separate charges, if applicable:</strong><br />
  a. <a id="rent-stabilized-rider-anchor" class="annotation-link" data-toggle="modal" data-target="#other-charges">Air conditioner : $_________</a><br />
  b. <a id="rent-stabilized-rider-anchor" class="annotation-link" data-toggle="modal" data-target="#other-charges">Appliances 	: $ _________</a>

 </div>

  <div class="col">
  c. <a id="rent-stabilized-rider-anchor" class="annotation-link" data-toggle="modal" data-target="#other-charges">421a (2.2%): $_________ <br />	Total separate charges: $_____________</a>  <br />
  d. <a id="rent-stabilized-rider-anchor" class="annotation-link" data-toggle="modal" data-target="#other-charges">Other: ________________$_________</a>

  </div>
</div>

<div class="mt-3">
<strong>5. Lower Rent to be charged, if any</strong>. 1 year lease <a id="rent-stabilized-rider-anchor" class="annotation-link" data-toggle="modal" data-target="#preferential-rent">$__________,</a> 2 year lease <a id="rent-stabilized-rider-anchor" class="annotation-link" data-toggle="modal" data-target="#preferential-rent">$__________</a> <a id="rent-stabilized-rider-anchor" class="annotation-link" data-toggle="modal" data-target="#preferential-rent-riders">Agreement attached: <a /> Yes [] No []
</div>

<div class="mt-3">
<strong>6. Tenant shall pay a <a id="rent-stabilized-rider-anchor" class="annotation-link" data-toggle="modal" data-target="#final-monthly-payment"> monthly rent (enter amount from 2F or 5) of $______________ for a 1 year renewal or $ ___________ for a 2 year renewal, plus total separate charges (enter amount from 4) $_____________ for a total monthly payment of $_____________ for a 1 year renewal or $____________ for a 2 year renewal.</a></strong>
</div>

<div class="mt-3">
7. This renewal lease shall commence on <a id="rent-stabilized-rider-anchor" class="annotation-link" data-toggle="modal" data-target="#new-lease-commencement">____________________</a>, which shall not be less than 90 days nor more than 150 days from the date of mailing or personal delivery of this Renewal Lease Form. This Renewal Lease shall terminate on
<a id="rent-stabilized-rider-anchor" class="annotation-link" data-toggle="modal" data-target="#new-lease-commencement">___________________</a> (1 year lease) or <a id="rent-stabilized-rider-anchor" class="annotation-link" data-toggle="modal" data-target="#new-lease-commencement">____________________</a> (2 year lease.)
</div>

<div class="mt-3">
8. This renewal lease is based on the same terms and conditions as your expiring lease. (See instructions about additional provisions.)
</div>

<div class="mt-3">
<a class="annotation-link" data-toggle="modal" data-target="#scrie-and-drie-frozen-rent">9. SCRIE and DRIE. </a> Owner and Tenant acknowledge that, as of the date of this renewal, Tenant is entitled to pay a reduced monthly rent in the amount of $ _______________ under the New York City SCRIE program or the New York City DRIE program. The reduced rent may be adjusted by orders of such program.
</div>

<div class="mt-3">
<strong>10. Leased premises does, does not have an <a class="annotation-link" data-toggle="modal" data-target="#operative-sprinkler-system">operative sprinkler system. <a /> If operative, it was last maintained and inspected on ____________________. This form becomes a binding lease renewal when signed by the owner below and returned to the tenant. A rider setting forth the rights and obligations of tenants and owners under the Rent Stabilization Law must be attached to this lease when signed by the owner and returned to the tenant. The rent, separate charges and total payment provided for in this renewal lease may be increased or decreased by order or annual updates of the Division of Housing and Community Renewal (DHCR) or the Rent Guidelines Board (RGB).</strong>

</div>
<div style= "background-color: #9B9B9B";>
<div class="border p-1 p-sm-4 p-xl-1 mt-5 text-center">
  <strong>PART B - TENANT'S RESPONSE TO OWNER</strong>
</div>
</div>

<div class="mt-3">
Tenant: Check and complete where indicated one of three responses below after reading instructions on reverse side. Then date and sign your response below. You must return this Renewal Lease Form to the owner in person or by regular mail, within 60 days of the date this Notice was served upon you by the owner. Your failure to do so may be grounds for the commencement of an action by the owner to evict you from your apartment.
</div>

<div class="mt-3">
&#9744; <a class="annotation-link" data-toggle="modal" data-target="#tenants-terms-of-acceptance">I (we), the undersigned Tenant(s), <a /> accept the offer of a one (1) year renewal lease at a monthly rent of $_______________ , plus separate charges of $_______________ for a total monthly payment of $_______________.<br />
&#9744;<a class="annotation-link" data-toggle="modal" data-target="#tenants-terms-of-acceptance">I (we), the undersigned Tenants(s), <a/> accept the offer of a two (2) year renewal lease at a monthly rent of $_______________, plus separate charges of $_______________ for a total monthly payment of $_______________.<br />
&#9744;I (we) will not renew my (our) lease and I (we) intend to vacate the apartment on the expiration date of the present lease.</a>

</div>

<div class="row mt-3">
  <div class="col">
  <a class="annotation-link" data-toggle="modal" data-target="#date-of-acceptance">Dated________________</a>

 </div>
  <div class="col">
  <a class="annotation-link" data-toggle="modal" data-target="#acknowledgement-of-acceptance">Tenant's Signature(s):________________</a>
  </div>
</div>

<div class="row mt-3">
  <div class="col">
  <a class="annotation-link" data-toggle="modal" data-target="#date-of-acceptance">Dated________________</a>
 </div>
  <div class="col">
  <a class="annotation-link" data-toggle="modal" data-target="#acknowledgement-of-acceptance">Owner's Signature(s):________________</a>
  </div>
</div>
</div>
<div class="page">
<div style= "color: #9B9B9B" class="text-center">
  Page 2 of 2
  </div>

<div style= "background-color: #9B9B9B";>
<div class="border p-1 p-sm-4 p-xl-1 mt-3 text-center">
  <strong> INSTRUCTIONS TO OWNER</strong>
</div>
</div>
<div class="mt-3">

&emsp; At least two copies of this completed Renewal Lease Form must be mailed to the tenant in occupancy or personally delivered not more than 150 days and not less than 90 days prior to the end of the tenant's lease term. <br />

&emsp; If the owner offers a <a class="annotation-link" data-toggle="modal" data-target="#timing-of-lease-renewal"> Renewal Lease less than 90 days prior to the expiration of the existing lease, </a> the lease term selected by the tenant shall begin at the tenant's option either (1) on the date a renewal lease would have begun had a timely offer been made or (2) on the first rent payment date occurring no less than 90 days after the date that the owner does offer the lease to the tenant. The guidelines rate applicable for such lease shall be no greater than the rate in effect on the commencement date of the lease for which a timely offer should have been made, and the increased rental shall not begin before the first rent payment date occurring no less than 90 days after such offer is made. <br />

&emsp; <a class="annotation-link" data-toggle="modal" data-target="#correct-rent-increases-and-registered-mcis">The owner must fully complete PART A on the reverse side of this Form explaining how the new rent has been computed.</a> Any rent increase must not exceed the applicable Rent Guidelines Board adjustment(s) plus other adjustments authorized by the Rent Stabilization Code. <br />

&emsp; Failure to file a current registration for this housing accommodation with DHCR bars the collection of any guidelines increase after such registration should have been filed. This sanction is lifted prospectively upon the filing of a proper registration. <br />

&emsp; <a class="annotation-link" data-toggle="modal" data-target="#no-new-lease-provisions">This Renewal Lease must be offered on the same terms and conditions as the expiring lease, </a> except for such additional provisions as are permitted by law or the Rent Stabilization Code which must be set forth by the owner and attached to this Form. If there are any additional lawful agreements between the owner and tenant, a copy signed by both parties must be attached to this Form. <br />

&emsp; The tenant must return to the owner all copies of this Form, completed and signed by the tenant in <strong>PART B</strong> on the reverse side of this Form. The owner must furnish the tenant with a fully executed copy of this Renewal Lease Form bearing the tenant's and owner's signatures in <strong>PART B</strong>, and a copy of the DHCR New York City Lease Rider, within 30 days of the owner's receipt of this Form signed by the tenant. Service of this fully executed Form, upon the tenant, constitutes a binding renewal lease.</a> <br />

&emsp; <a class="annotation-link" data-toggle="modal" data-target="#return-copy">If the owner fails to furnish the tenant with a fully executed copy of this Form within 30 days of receipt of the Form signed by the tenant, the tenant shall continue to have all rights</a> afforded by the Rent Stabilization Law and Code, and the owner will be barred from commencing any action or proceeding against the tenant based upon non-renewal of lease. <br />
</div>
<div style= "background-color: #9B9B9B";>
<div class="border p-1 p-sm-4 p-xl-1 mt-5 text-center">
  <strong> INSTRUCTIONS TO TENANT</strong> <br />
  (Read Owner's and Tenant's Instructions carefully before completing this Renewal Lease Form)
</div>
</div>

<div class="mt-3">
&emsp; <a class="annotation-link" data-toggle="modal" data-target="#choice-to-reject-or-accept-offer">If you wish to accept this offer to renew your lease</a>, you must complete and sign this Renewal Lease Form in the space provided in PART B on the reverse side of this Form, and you must return all copies of the signed Lease Form to the owner in person or by regular mail within 60 days of the date this Form was served upon you. You may wish to make a copy for your own records. <i>If you are the recipient of a Senior Citizen Rent Increase Exemption, or a Disability Rent Increase Exemption, your monthly rent is listed in item 9 and you must select a one-or two-year lease, or you will lose this exemption.</i> <br />

&emsp; Before you complete and sign <strong>PART B</strong> and return this Renewal Lease Form, be sure to check that all lawful provisions and written agreements have been attached by the owner to this Form. Please read all attachments carefully. If such other lawful provisions appear, they are part of this lease renewal offer and renewal lease. If there are any lawful agreements between you and the owner, attached copies must be signed by both parties. <br />

&emsp; <a class="annotation-link" data-toggle="modal" data-target="#questions-or-concerns">If you question the Legal Regulated Rent or the rental adjustments specified on the front of this Renewal Lease Form, ask the owner for an explanation. Or, you may telephone the NYS Division of Housing and Community Renewal (DHCR), Office of Rent Administration, at (718) 739-6400, before the end of the 60 day period from the date this Form was served upon you.</a> <br />

&emsp; If the owner agrees to a rent which is lower than the legal regulated rent, this lower amount should be entered in item 5 on the reverse side of this Form, and a signed copy of the agreement should be attached. You may not change the content of this Renewal Lease Form without the owner's written consent. <a id="rent-stabilized-rider-anchor" class="annotation-link" data-toggle="modal" data-target="#preferential-rent-notice">If a "lower rent" amount is listed in item 5 and such rent is a "preferential rent", upon renewal the owner may increase the rent to the legal rent listed in item 2 plus all subsequent lawful adjustments.</a> <br />

&emsp; <a class="annotation-link" data-toggle="modal" data-target="#conditions-of-acceptance-and-eviction">Your acceptance of this offer to renew shall constitute a renewal of the present lease for the term of years and rent accepted, subject to any other lawful changes which appear in writing on the attachments to this Form, and subject also to payment of the new rent and additional security, if any. Such additional security shall be deposited by the owner in the manner provided for on initial occupancy.</a> <br />

&emsp; If you do not sign and return this Renewal Lease Form at the new rent (which appears in column "F" or item 5 on the reverse side of this Form) in accordance with the instruction, and within the prescribed 60 day period, the owner may have grounds to start proceedings to evict you from your apartment. <br />

&emsp; You may file a complaint with DHCR, if you have not received a copy of the DHCR Lease Rights Rider with this Renewal Lease Form when signed and returned to you by the owner, or if you do not receive a fully executed copy of this Renewal Lease within 30 days from the owner's receipt of a copy of this Form signed by you.

</div>

<div class="mt-3 text-center">
State of New York <br />
Division of Housing and Community Renewal Office of Rent Administration/Gertz Plaza 92-31 Union Hall Street<br />
Jamaica, New York 11433<br />
Web Site: www.nyshcr.org

</div>


</div>
