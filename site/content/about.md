---
title: About
description: About
slug: about
menu: main
name: “About”
weight: 4
---
**Lease**
This is an annotation of the standard lease that a landlord would extend to a prospective tenant of a rent-stabilized unit.

**Rider**
This is an annotation of the rider that should be attached to all leases that are extended to a prospective or a returning renter of a rent-stabilized unit.

**Renewal**
This is an annotation of the renewal lease that is extended to a returning renter of a rent-stabilized unit.

